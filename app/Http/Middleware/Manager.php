<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::find($request->route('id')) !==null and User::find($request->route('id'))->responsible_user_id == Auth::user()->id or Auth::user()->is_admin()){
            return $next($request);
        }else{
            return view('errors.fail', ['errormsg' => 'Не жульничай это не твой клиент', 'link' => env('APP_URL')]);
        }
    }
}
