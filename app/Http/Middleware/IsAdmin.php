<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if(!Auth::user()->is_admin() and !Auth::user()->is_manager()){
            return view('errors.fail', ['errormsg' => 'У Вас нету доступа к этой странице', 'link' => '/']);
        }else{
            if(Auth::user()->is_manager() and (strpos($request->getPathInfo(), '/admin/manager') !== false) ){
                return view('errors.fail', ['errormsg' => 'У Вас нету доступа к этой странице', 'link' => '/']);
            }
            return $next($request);
        }
    }
}