<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Module
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->is_admin()){
            return $next($request);
        }
        $account = explode('.', $request->getHost())[0];
        if ($account != Auth::user()['domain_id']) {
            Auth::logout();
            return redirect(env('APP_URL'));
        }else {
            return $next($request);
        }
    }
}
