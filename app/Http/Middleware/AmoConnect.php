<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Auth;
require_once app_path().'/Amo/map.php';

class AmoConnect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(!isset($user)){
            return view('errors.fail', ['errormsg' => 'Такого пользователя \ аккаунта не существует', 'link' => env('APP_URL')]);
        }

        $amo = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain );

        if(!$amo){
            return view('errors.fail', ['errormsg' => 'Нет подключения к amoCRM, заполните/проверте данные подключения', 'link' => '/settings']);
        }

        if($amo->getAccountInfo()===false){
            return view('errors.fail', ['errormsg' => 'Нет подключения к amoCRM, заполните/проверте данные подключения', 'link' => '/settings']);
        }
        return $next($request);
    }
}
