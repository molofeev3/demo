<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('install', ['uses'=>'InitController@index']);
Route::post('init', ['uses'=>'InitController@init']);

Route::get('/', ['uses'=>'DashboardController@show']);
Route::get('/users', ['uses'=>'DashboardController@users']);
Route::post('/users/new', ['uses'=>'DashboardController@users_new']);
Route::any('/user/{id}', ['uses'=>'DashboardController@user_control'])->where(['id'=>'[0-9]+']);
Route::get('/settings', ['uses'=>'DashboardController@settings']);
Route::post('/settings/update', ['uses'=>'DashboardController@settings_update']);

Route::get('modules', ['uses' => 'DashboardController@modules']);
Route::get('modules/on/{name}',  function($name)
{
    $app = app();
    $controller = $app->make('\App\Http\Controllers\Modules\\'.$name.'\Controllers\MainController');
    return $controller->callAction('install', $parameters = array());
});
Route::get('modules/off/{name}',  function($name)
{
    $app = app();
    $controller = $app->make('\App\Http\Controllers\Modules\\'.$name.'\Controllers\MainController');
    return $controller->callAction('uninstall', $parameters = array());
});
Route::post('module/upload', ['uses'=>'DashboardController@moduleUpload']);


Route::auth();