<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class InitController extends Controller
{
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
    }

    public function index(){
        if(Schema::hasTable('users')){
            return redirect('/');
        }
        return view('init');
    }

    public function init(){
        $input = $this->request->all();
        $this->validate($this->request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|confirmed'
        ]);
        Artisan::call('migrate');
        Artisan::call('db:seed');
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
        ]);
        $user->roles()->attach(2);
        $user->save();
        return redirect('/');
    }
}
