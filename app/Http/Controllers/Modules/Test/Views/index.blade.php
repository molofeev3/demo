@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <section id="main-content">
        <section class="wrapper">
            <h1>Test module for example</h1>

            @foreach($test_data as $data)
                <p>{{$data['column']}}</p>
            @endforeach
        </section>
    </section>

@endsection