<?php
/**
 * Created by PhpStorm.
 * User: molofeev
 * Date: 17.05.17
 * Time: 15:56
 */
Route::group(['namespace' => 'Modules\Test\Controllers'], function()
{
    Route::get('/hello', ['uses' =>'TestController@index']); // example module routing
});
