<?php
/*
 *
 * Example of defining Model class
 *
 */
namespace App\Http\Controllers\Modules\Test;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Test extends Authenticatable
{
    protected $table = 'test';

    protected $fillable = [
        'column'
    ];

}
