<?php

namespace App\Http\Controllers\Modules\Test\Controllers; // define namespace


use App\Http\Controllers\Controller; // use base controller class
use App\Http\Controllers\Modules\Test\Test; // including Model class


class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//         dd(Test::all()); // example of use model class
        return view('Test.Views.index', array('test_data' => Test::all()->toArray())); // example of use view
    }
}
