<?php

namespace App\Http\Controllers\Modules\Test\Controllers; // define namespace


use App\Http\Controllers\Controller;
use App\Menu;
use App\Module;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema; // use base controller class


class MainController extends Controller // example of main controller for install and uninstall module
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function install()
    {
        Schema::create('test', function(Blueprint $table) // example of creating DB column
        {
            $table->increments('id');
            $table->string('column');
        });

        $menu_item = new Menu(); // example of add module to menu
        $menu_item->title = 'Test';
        $menu_item->url = 'hello';
        $menu_item->ico = 'fa-pencil';
        $menu_item->save();

        $module = new Module();// example of registration module
        $module->name = 'Test';
        $module->save();

        return redirect('/modules');
    }

    public function uninstall(){
        Schema::drop('test');// example of deleting DB column

        Menu::where('title', '=', 'Test')->delete(); //example of removing module from menu

        Module::where('name', '=', 'Test')->delete(); // example of understating module

        return redirect('/modules');
    }
}
