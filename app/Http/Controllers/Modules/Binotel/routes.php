<?php
/**
 * Created by PhpStorm.
 * User: molofeev
 * Date: 17.05.17
 * Time: 15:56
 */
Route::group(['namespace' => 'Modules\Binotel\Controllers'], function()
{
    Route::get('/binotel', ['uses' => 'BinotelController@show']);
    Route::post('/binotel/update', ['uses' => 'BinotelController@update']);
    Route::post('/binotel/webhook', ['uses' => 'BinotelController@webhook']);
});