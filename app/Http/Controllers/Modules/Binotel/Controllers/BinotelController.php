<?php

namespace App\Http\Controllers\Modules\Binotel\Controllers;

use App\Http\Controllers\Modules\Binotel\Binotel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
require_once app_path().'/Amo/map.php';

class BinotelController extends Controller
{
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->middleware('auth', ['except' => [
            'webhook'
        ]]);
        $this->middleware('amo', ['except' => [
            'webhook'
        ]]);
        $this->request = $request;
    }

    public function show(){
        $user = Auth::user();
        $amo = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain);
        $amo_account = $amo->getAccountInfo()['account'];
        #todo
        $data = array(
            'binotel' => Binotel::firstOrCreate([]),
            'amo_fields' => $amo_account['custom_fields'],
            'managers' => $amo_account['users'],
            'leads_statuses' => $amo_account['pipelines'],
            'user' => $user
        );
        return view('Binotel.Views.index', $data);
    }

    public function update(){
        $user = Auth::user();

        $input = $this->request->all();
        $this->validate($this->request, [
            'company_id' => 'required|integer'
        ]);
        $binotel = Binotel::firstOrCreate([]);
        $binotel->company_id = $input['company_id'];
        $binotel->user_id = $user->id;
        $binotel->utm_target  = serialize($input['utm_target']);
        $binotel->cases = serialize($input['case']);
        $phones = array();
        foreach ($input['phones']['phone'] as $key => $phone){
            $phones[$phone] = $input['phones']['manager'][$key];
        }
        $binotel->phones = serialize($phones);
        $binotel->utm = serialize($input['utm']);
        $binotel->phone_field_id = $input['phone_field_id'];
        $binotel->save();
        return redirect('/binotel');
    }

    private function createNote(\AmoCRM $amo_crm, $contact, $manager, $external_number, $callDetails, $call_type){
        $note = $amo_crm->createNoteCallInstance();
        $note->setNoteType(10);//for input call
        $note->setElementType($contact['type'] == 'company' ? 3 : 1); // company or contact
        $note->setCallStatus(4);//	Разговор состоялся
        $note->setElementId($contact['id']);
        $note->setLastModified(time());
        $note->setCreatedUserId($manager); // responsible user
        $note->setUNIQ(md5($external_number . date('Y-m-d H:i:s')));
        $note->setLINK('https://my.binotel.ua/?module=cdrs&action=generateFile&fileName=' . $callDetails['callID'] . '.mp3&callDate=' . date('H-i_d-m-Y', $callDetails['startTime']) . '&customerNumber=' . $callDetails['externalNumber'] . '&callType=incoming');
        $note->setPHONE($external_number);
        $note->setDURATION($callDetails['billsec']);
        $note->setSRC('amo_binotel');
        $note->setCallStatus(4);
        $note->add();
    }

    private function createContact(\AmoCRM $amo_crm, $contact_name, $manager, $external_number, $phone_field_id, $linked_lead_id=null){
        $contact = $amo_crm->createContactInstance();
        $contact->setName($contact_name);
        $contact->setResponsibleUserId($manager);
        if($linked_lead_id != null){
            $contact->setLinkedLeadsId(array($linked_lead_id));
        }
        $contactCF = new \CustomFields();
        $contactCF->push(new \CustomField('8', $external_number, $phone_field_id));
        $contact->setCustomFields($contactCF);
        return $contact->add();
    }

    private function createTask(\AmoCRM $amo_crm, $element_id, $element_type, $responsible_id, $task_type, $task_text){
        $task = $amo_crm->createTaskInstance();
        $task->setElementId($element_id);
        $task->setElementType($element_type);
        $task->setResponsibleUserId($responsible_id);
        #todo task type
        $task->setTaskType($task_type);
        $task->setText($task_text);
        $task->setCompleteTill(date('23:59:59 d-m-Y',time()));
        $task->add();
    }
    private function createLead(\AmoCRM $amo_crm, $responsible_id, $lead_name, $lead_status){
        $lead = $amo_crm->createLeadInstance();
        $lead->setResponsibleUserId($responsible_id);
        $lead->setName($lead_name);
        $lead->setStatus($lead_status);
        //custom fields for UTM terms
        return $lead->add();
    }
    private function is_contactCf(\AmoCRM $amo_crm, $field_id){
        $flag = false;
        foreach ($amo_crm->getAccountInfo()['account']['custom_fields']['contacts'] as $custom_field){
            if(isset($custom_field['id']) and $custom_field['id'] == $field_id){
                $flag = true;
            }
        }
        return $flag;
    }

    private function is_leadCf(\AmoCRM $amo_crm, $field_id){
        $flag = false;
        foreach ($amo_crm->getAccountInfo()['account']['custom_fields']['leads'] as $custom_field){
            if(isset($custom_field['id']) and $custom_field['id'] == $field_id){
                $flag = true;
            }
        }
        return $flag;
    }

    private function getCustomFieldType(\AmoCRM $amo_crm, $field_id, $element_type){
        $type_id = false;
        foreach ($amo_crm->getAccountInfo()['account']['custom_fields'][$element_type] as $custom_field){
            if(isset($custom_field['id']) and $custom_field['id'] == $field_id){
                $type_id = $custom_field['type_id'];
            }
        }
        return $type_id;
    }

    private function setUtm(\AmoCRM $amo_crm, $element_id, $element_type, $utm_data, $binotel){
        if($element_type == 1){
            //update contact
            $contact = $amo_crm->createContactInstance();
            $contact->setId($element_id);
            $contactCF = new \CustomFields();
            //work with utm
            $utm_settings = unserialize($binotel->utm);
            foreach ($utm_settings as $term => $crm_id){
                if($crm_id != 0 and $this->is_contactCf($amo_crm, $crm_id) and isset($utm_data[$term])){
                    $contactCF->push(new \CustomField($this->getCustomFieldType($amo_crm, $crm_id, 'contacts'), $utm_data[$term], $crm_id));
                }
            }
            $contact->setCustomFields($contactCF);
            $contact->update();
        }elseif ($element_type == 2){
            // update lead
            $lead = $amo_crm->createLeadInstance();
            $lead->setId($element_id);
            //work with utm
            $leadCF = new \CustomFields();
            //work with utm
            $utm_settings = unserialize($binotel->utm);
            foreach ($utm_settings as $term => $crm_id){
                if($crm_id != 0 and $this->is_leadCf($amo_crm, $crm_id) and isset($utm_data[$term])){
                    $leadCF->push(new \CustomField($this->getCustomFieldType($amo_crm, $crm_id, 'leads'), $utm_data[$term], $crm_id));
                }
            }
            $lead->setCustomFields($leadCF);
            $lead->update();
        }
    }

    private function getManagerIdByEmail($email, $data){
        $id = null;
        foreach ($data as $user){
            if($user['login'] == $email){
                $id = $user['id'];
            }
        }
        return $id;
    }

    private function setUtmToNote(\AmoCRM $amoCRM, $element_type, $element_id, $utm_data){
        $note = $amoCRM->createNoteInstance();
        $note->setElementType($element_type);
        $note->setElementId($element_id);
        $text = '';
        foreach ($utm_data as $term => $value){
            $text .= $term.': '.$value.PHP_EOL;
        }
        $note->setText($text);
        $note->add();
    }

    public function webhook(){
        $input = $this->request->all();
        if(isset($input['requestType']) and $input['requestType'] == 'apiCallCompleted'){
            $callDetails = $input['callDetails'];
            $company_id = $callDetails['companyID'];
            $call_type = $callDetails['callType']; // 0 - input call, 1 - output call
            $disposition = ($callDetails['disposition'] == 'ANSWER') ? 1 : 0; // 1-answered call, 0-unanswered call
            $internal_number = $callDetails['internalNumber'];
            $external_number = $callDetails['externalNumber'];

            $binotel = Binotel::where('company_id', $company_id)->first();
            $user = $binotel->getUser();
            $amo_crm = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain);

            $manager = isset(unserialize($binotel->phones)[$internal_number]) ? unserialize($binotel->phones)[$internal_number] : $this->getManagerIdByEmail($user->amo_email, $amo_crm->getAccountInfo()['account']['users']) ;
            $contact = $amo_crm->getContactByPhones(array($external_number));
            $cases = unserialize($binotel->cases);
            $phone_field_id = $binotel->phone_field_id;

            $utm_target = unserialize($binotel->utm_target);

            if($call_type == 0){ // incoming call

                if( isset($callDetails['callTrackingData']) and is_array($callDetails['callTrackingData']) and !empty($callDetails['callTrackingData']) ){ // incoming call with calltracking
                    if($disposition == 1){ // answered
                        if($contact){ // contact exist
                            //Input call, was answered, contact exist in amoCRM
                            //Default: Add talk record to contact
                            $this->createNote($amo_crm, $contact, $manager, $external_number, $callDetails, 10);
                            echo '{"status":"success"}';
                        }else{ // new contact
                            //Input call, was answered, contact not exist in amoCRM
                            //Choise: 1)Create contact and task in. 2)Create contact, lead and task in. 3) Nothing to do.
                            if($cases['calltracking']['answer']['no_contact']['value'] == 1){ // case 1
                                $contact_id = $this->createContact($amo_crm, $cases['calltracking']['answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                if($contact_id != null){
                                    if(isset($utm_target['entity']) and $utm_target['entity'] == 'contacts'){
                                        if(isset($utm_target['place']) and $utm_target['place'] == 'notice'){
                                            $this->setUtmToNote($amo_crm, 1, $contact_id, $callDetails['callTrackingData']);
                                        }elseif(isset($utm_target['place']) and $utm_target['place'] == 'fields'){
                                            $this->setUtm($amo_crm, $contact_id, 1, $callDetails['callTrackingData'], $binotel);
                                        }
                                    }
                                    $this->createNote($amo_crm, array('type' => 'contact', 'id' => $contact_id), $manager, $external_number, $callDetails, 10);
                                    $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['calltracking']['answer']['no_contact'][1]['task_text']);
                                }
                            }elseif($cases['calltracking']['answer']['no_contact']['value'] == 2){
                                $lead_id = $this->createLead($amo_crm, $manager, $cases['calltracking']['answer']['no_contact'][2]['lead_name'], $cases['calltracking']['answer']['no_contact'][2]['lead_status']);
                                if($lead_id != null){
                                    if(isset($utm_target['entity']) and $utm_target['entity'] == 'leads'){
                                        if(isset($utm_target['place']) and $utm_target['place'] == 'notice'){
                                            $this->setUtmToNote($amo_crm, 2, $lead_id, $callDetails['callTrackingData']);
                                        }elseif(isset($utm_target['place']) and $utm_target['place'] == 'fields'){
                                            $this->setUtm($amo_crm, $lead_id, 2, $callDetails['callTrackingData'], $binotel);
                                        }
                                    }
                                    $contact_id = $this->createContact($amo_crm, $cases['calltracking']['answer']['no_contact'][2]['contact_name'], $manager, $external_number, $phone_field_id, $lead_id);
                                    $this->createNote($amo_crm, array('type' => 'contact', 'id' => $contact_id), $manager, $external_number, $callDetails, 10);
                                    $this->createTask($amo_crm, $lead_id, 2, $manager, 1, $cases['calltracking']['answer']['no_contact'][2]['task_text']);
                                }
                            }elseif ($cases['calltracking']['answer']['no_contact']['value'] == 3){

                            }
                            echo '{"status":"success"}';
                        }
                    }else{ // not answer
                        if($contact){ // contact exist
                            //Input call, was not answered, contact exist in amoCRM
                            //Default: Create task in contact "recall to contact"
                            $this->createTask($amo_crm, $contact['id'], 1, $manager, 1, $cases['calltracking']['no_answer']['contact_exist']['task_text']);
                            echo '{"status":"success"}';
                        }else{ // no contact
                            //Input call, was not answered, contact not exist in amoCRM
                            //Choise: 1)Create contact and task in. 2)Create contact, lead and task in. 3)Nothing to do
                            if($cases['calltracking']['no_answer']['no_contact']['value'] == 1){
                                $contact_id = $this->createContact($amo_crm, $cases['calltracking']['no_answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                if($contact_id != null){
                                    $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['calltracking']['no_answer']['no_contact'][1]['task_text']);
                                    if(isset($utm_target['entity']) and $utm_target['entity'] == 'contacts'){
                                        if(isset($utm_target['place']) and $utm_target['place'] == 'notice'){
                                            $this->setUtmToNote($amo_crm, 1, $contact_id, $callDetails['callTrackingData']);
                                        }elseif(isset($utm_target['place']) and $utm_target['place'] == 'fields'){
                                            $this->setUtm($amo_crm, $contact_id, 1, $callDetails['callTrackingData'], $binotel);
                                        }
                                    }
                                }
                            }elseif($cases['calltracking']['no_answer']['no_contact']['value'] == 2){
                                $lead_id = $this->createLead($amo_crm, $manager, $cases['calltracking']['no_answer']['no_contact'][2]['lead_name'], $cases['calltracking']['no_answer']['no_contact'][2]['lead_status']);
                                if($lead_id != null){
                                    $contact_id = $this->createContact($amo_crm, $cases['calltracking']['no_answer']['no_contact'][2]['contact_name'], $manager, $external_number, $phone_field_id, $lead_id);
                                    $this->createTask($amo_crm, $lead_id, 2, $manager, 1, $cases['calltracking']['no_answer']['no_contact'][2]['task_text']);
                                    if(isset($utm_target['entity']) and $utm_target['entity'] == 'leads'){
                                        if(isset($utm_target['place']) and $utm_target['place'] == 'notice'){
                                            $this->setUtmToNote($amo_crm, 2, $lead_id, $callDetails['callTrackingData']);
                                        }elseif(isset($utm_target['place']) and $utm_target['place'] == 'fields'){
                                            $this->setUtm($amo_crm, $lead_id, 2, $callDetails['callTrackingData'], $binotel);
                                        }
                                    }elseif (isset($utm_target['entity']) and $utm_target['entity'] == 'contacts'){
                                        if(isset($utm_target['place']) and $utm_target['place'] == 'notice'){
                                            $this->setUtmToNote($amo_crm, 1, $contact_id, $callDetails['callTrackingData']);
                                        }elseif(isset($utm_target['place']) and $utm_target['place'] == 'fields'){
                                            $this->setUtm($amo_crm, $contact_id, 1, $callDetails['callTrackingData'], $binotel);
                                        }
                                    }
                                }
                            }elseif ($cases['calltracking']['no_answer']['no_contact']['value'] == 3){

                            }
                            echo '{"status":"success"}';
                        }
                    }
                }else{ // simple incoming call

                    if($disposition == 1){ // answered
                        if($contact){ // contact exist
                            //Input call, was answered, contact exist in amoCRM
                            //Default: Add talk record to contact
                            $this->createNote($amo_crm, $contact, $manager, $external_number, $callDetails, 10);
                            echo '{"status":"success"}';
                        }else{ // new contact
                            //Input call, was answered, contact not exist in amoCRM
                            //Choise: 1)Create contact and task in. 2)Create contact, lead and task in. 3) Nothing to do.
                            if($cases['input']['answer']['no_contact']['value'] == 1){ // case 1
                                $contact_id = $this->createContact($amo_crm, $cases['input']['answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                if($contact_id != null){
                                    $this->createNote($amo_crm, array('type' => 'contact', 'id' => $contact_id), $manager, $external_number, $callDetails, 10);
                                    $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['input']['answer']['no_contact'][1]['task_text']);
                                }
                            }elseif($cases['input']['answer']['no_contact']['value'] == 2){
                                $lead_id = $this->createLead($amo_crm, $manager, $cases['input']['answer']['no_contact'][2]['lead_name'], $cases['input']['answer']['no_contact'][2]['lead_status']);
                                if($lead_id != null){
                                    $contact_id = $this->createContact($amo_crm, $cases['input']['answer']['no_contact'][2]['contact_name'], $manager, $external_number, $phone_field_id, $lead_id);
                                    $this->createNote($amo_crm, array('type' => 'contact', 'id' => $contact_id), $manager, $external_number, $callDetails, 10);
                                    $this->createTask($amo_crm, $lead_id, 2, $manager, 1, $cases['input']['answer']['no_contact'][2]['task_text']);
                                }
                            }elseif ($cases['input']['answer']['no_contact']['value'] == 3){

                            }
                            echo '{"status":"success"}';
                        }
                    }else{ // not answer
                        if($contact){ // contact exist
                            //Input call, was not answered, contact exist in amoCRM
                            //Default: Create task in contact "recall to contact"
                            $this->createTask($amo_crm, $contact['id'], 1, $manager, 1, $cases['input']['no_answer']['contact_exist']['task_text']);
                            echo '{"status":"success"}';
                        }else{ // no contact
                            //Input call, was not answered, contact not exist in amoCRM
                            //Choise: 1)Create contact and task in. 2)Create contact, lead and task in. 3)Nothing to do
                            if($cases['input']['no_answer']['no_contact']['value'] == 1){
                                $contact_id = $this->createContact($amo_crm, $cases['input']['no_answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                if($contact_id != null){
                                    $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['input']['no_answer']['no_contact'][1]['task_text']);
                                }
                            }elseif($cases['input']['no_answer']['no_contact']['value'] == 2){
                                $lead_id = $this->createLead($amo_crm, $manager, $cases['input']['no_answer']['no_contact'][2]['lead_name'], $cases['input']['no_answer']['no_contact'][2]['lead_status']);
                                if($lead_id != null){
                                    $this->createContact($amo_crm, $cases['input']['no_answer']['no_contact'][2]['contact_name'], $manager, $external_number, $phone_field_id, $lead_id);
                                    $this->createTask($amo_crm, $lead_id, 2, $manager, 1, $cases['input']['no_answer']['no_contact'][2]['task_text']);
                                }
                            }elseif ($cases['input']['no_answer']['no_contact']['value'] == 3){

                            }
                            echo '{"status":"success"}';
                        }
                    }
                }
            }else{ //outcoming call
                if(isset($callDetails['getCallData']) and is_array($callDetails['getCallData']) and !empty($callDetails['getCallData'])){//if outcoming call via GetCall
                    if($disposition == 1){//answered
                        if($contact){ //contact exist
                            //Output call, was answered, contact exist in amoCRM
                            //Default: Add talk record to contact
                            $this->createNote($amo_crm, $contact, $manager, $external_number, $callDetails, 11);
                            echo '{"status":"success"}';
                        }else{ // no contact
                            //Output call, was answered, contact not exist in amoCRM
                            //Choise: 1)Create task in contact "recall to contact" 2)Nothing to do
                            if($cases['getcall']['answer']['no_contact']['value'] == 1){
                                $contact_id = $this->createContact($amo_crm, $cases['getcall']['answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                if(isset($utm_target['entity']) and $utm_target['entity'] == 'contacts'){
                                    if(isset($utm_target['place']) and $utm_target['place'] == 'notice'){
                                        $this->setUtmToNote($amo_crm, 1, $contact_id, $callDetails['callTrackingData']);
                                    }elseif(isset($utm_target['place']) and $utm_target['place'] == 'fields'){
                                        $this->setUtm($amo_crm, $contact_id, 1, $callDetails['callTrackingData'], $binotel);
                                    }
                                }
                                $this->createNote($amo_crm, array('type' => 'contact', 'id' => $contact_id), $manager, $external_number, $callDetails, 11);
                                $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['getcall']['answer']['no_contact'][1]['task_text']);
                            }
                            echo '{"status":"success"}';
                        }
                    }else{//no answer
                        if($contact){//contact exist
                            //Output call, was not answered, contact exist in amoCRM
                            //Default: Nothing to do
                            echo '{"status":"success"}';
                        }else{//no contact
                            //Output call, was not answered, contact not exist in amoCRM
                            //Choise: 1)Create task in contact "recall to contact" 2)Nothing to do
                            if($cases['getcall']['no_answer']['no_contact']['value'] == 1){
                                $contact_id = $this->createContact($amo_crm, $cases['getcall']['no_answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['getcall']['no_answer']['no_contact'][1]['task_text']);
                                if(isset($utm_target['entity']) and $utm_target['entity'] == 'contacts'){
                                    if(isset($utm_target['place']) and $utm_target['place'] == 'notice'){
                                        $this->setUtmToNote($amo_crm, 1, $contact_id, $callDetails['callTrackingData']);
                                    }elseif(isset($utm_target['place']) and $utm_target['place'] == 'fields'){
                                        $this->setUtm($amo_crm, $contact_id, 1, $callDetails['callTrackingData'], $binotel);
                                    }
                                }
                            }
                            echo '{"status":"success"}';
                        }
                    }
                }else{// if outcoming call is simple
                    if($disposition == 1){//answered
                        if($contact){ //contact exist
                            //Output call, was answered, contact exist in amoCRM
                            //Default: Add talk record to contact
                            $this->createNote($amo_crm, $contact, $manager, $external_number, $callDetails, 11);
                            echo '{"status":"success"}';
                        }else{ // no contact
                            //Output call, was answered, contact not exist in amoCRM
                            //Choise: 1)Create task in contact "recall to contact" 2)Nothing to do
                            if($cases['output']['answer']['no_contact']['value'] == 1){
                                $contact_id = $this->createContact($amo_crm, $cases['output']['answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                $this->createNote($amo_crm, array('type' => 'contact', 'id' => $contact_id), $manager, $external_number, $callDetails, 11);
                                $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['output']['answer']['no_contact'][1]['task_text']);
                            }elseif ($cases['output']['answer']['no_contact']['value'] == 2){

                            }
                            echo '{"status":"success"}';
                        }
                    }else{//no answer
                        if($contact){//contact exist
                            //Output call, was not answered, contact exist in amoCRM
                            //Default: Nothing to do
                            echo '{"status":"success"}';
                        }else{//no contact
                            //Output call, was not answered, contact not exist in amoCRM
                            //Choise: 1)Create task in contact "recall to contact" 2)Nothing to do
                            if($cases['output']['no_answer']['no_contact']['value'] == 1){
                                $contact_id = $this->createContact($amo_crm, $cases['output']['no_answer']['no_contact'][1]['contact_name'], $manager, $external_number, $phone_field_id);
                                $this->createTask($amo_crm, $contact_id, 1, $manager, 1, $cases['output']['no_answer']['no_contact'][1]['task_text']);
                            }elseif ($cases['output']['no_answer']['no_contact']['value'] == 2){

                            }
                            echo '{"status":"success"}';
                        }
                    }
                }
            }
        }
    }
}