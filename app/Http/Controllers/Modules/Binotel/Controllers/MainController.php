<?php

namespace App\Http\Controllers\Modules\Binotel\Controllers; // define namespace


use App\Http\Controllers\Controller;
use App\Menu;
use App\Module;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema; // use base controller class


class MainController extends Controller // example of main controller for install and uninstall module
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function install()
    {
        Schema::create('binotel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->longText('phones')->nullable();
            $table->longText('utm')->nullable();
            $table->longText('cases')->nullable();
            $table->integer('phone_field_id')->nullable();
            $table->integer('user_id');
            $table->longText('utm_target');
            $table->timestamps();
        });

        $menu_item = new Menu(); // example of add module to menu
        $menu_item->title = 'Интеграция с binotel';
        $menu_item->url = 'binotel';
        $menu_item->parent = 3; //subitem for Modules item, subitems hasnt ico
        $menu_item->save();

        $module = new Module();// example of registration module
        $module->name = 'Binotel';
        $module->save();

        return redirect('/modules');
    }

    public function uninstall(){
        Schema::drop('binotel');

        Menu::where('title', '=', 'Интеграция с binotel')->delete(); //example of removing module from menu

        Module::where('name', '=', 'Binotel')->delete(); // example of understating module

        return redirect('/modules');
    }
}
