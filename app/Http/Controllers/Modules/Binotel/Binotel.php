<?php

namespace App\Http\Controllers\Modules\Binotel;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Binotel extends Model
{
    protected $table = 'binotel';

    protected $fillable = [
        'company_id', 'phones', 'utm', 'cases', 'phone_field_id', 'user_id', 'utm_target'
    ];

    public function getUser(){

        return User::find($this->id);
    }
}