@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Интеграция с Бинотел</h1>
                    <?php
                    $cases = unserialize($binotel->cases);
                    $utm = unserialize($binotel->utm);


                    if(isset($utm) and is_array($utm)){
                        $options = array();
                        foreach ($utm as $term => $value){
                            $options[$term] = '';
                            $options[$term] .= '<option value="0">Выберите поле</option>';
                            foreach ($amo_fields as $key => $item){
                                $options[$term] .= '<optgroup label="'.$key.'">';
                                foreach ($item as $field){
                                    if($field['id'] == $value){
                                        $options[$term] .= '<option selected value="'.$field['id'].'">'.$field['name'].'</option>';
                                    }else{
                                        $options[$term] .= '<option value="'.$field['id'].'">'.$field['name'].'</option>';
                                    }
                                }
                                $options[$term] .= '</optgroup>';
                            }
                        }
                    }else{
                        $options = '';
                        $options .= '<option value="0">Выберите поле</option>';
                        foreach ($amo_fields as $key => $item){
                            $options .= '<optgroup label="'.$key.'">';
                            foreach ($item as $field){
                                $options .= '<option value="'.$field['id'].'">'.$field['name'].'</option>';
                            }
                            $options .= '</optgroup>';
                        }
                    }
                    ?>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/binotel/update') }}">
                        {{ csrf_field() }}
                        <section class="panel">
                            <header class="panel-heading">
                                Общие астройки
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4 form-group{{ $errors->has('company_id') ? ' has-error' : '' }}">
                                        <label for="company_id" class="col-md-8 control-label">Id компании(аккаунт в Бинотел)</label>
                                        <div class="col-md-4">
                                            <input id="name" required type="number" class="form-control" name="company_id" value="{{ !empty($binotel->company_id) ? $binotel->company_id : '' }}">
                                            @if ($errors->has('company_id'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('company_id') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="company_id" class="col-md-5 control-label">Поле телефон в CRM</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="phone_field_id" required>
                                                <option>Выберите поле</option>
                                                @foreach($amo_fields as $key => $item)
                                                    @if($key == 'contacts')
                                                        @foreach ($item as $field){
                                                        <option value="{{$field['id']}}" {{ (isset($binotel->phone_field_id) and $binotel->phone_field_id==$field['id']) ? 'selected' : '' }}>{{$field['name']}}</option>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-md-offset-3">
                                        <button class="btn btn-primary" type="submit">Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="panel">
                            <header class="panel-heading">
                                Типы звонков
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#input">Входящий</a></li>
                                            <li><a data-toggle="tab" href="#output">Исходящий</a></li>
                                            <li><a data-toggle="tab" href="#getcall">GetCall</a></li>
                                            <li><a data-toggle="tab" href="#calltracking">CallTracking</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div id="input" class="tab-pane fade in active">
                                                <h3>Отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                        </div>
                                                        <div class="col-md-4">
                                                            Добавление в существующий контакт записи разговора
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php
                                                            $case1 = isset($cases['input']['answer']['no_contact']) ? $cases['input']['answer']['no_contact'] : '';
                                                            ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[input][answer][no_contact][value]">
                                                                <option {{(isset($case1['value']) and $case1['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case1['value']) and $case1['value']==2) ? 'selected' : ''}} value="2">Создание контакта, сделки и задачи в ней</option>
                                                                <option {{(isset($case1['value']) and $case1['value']==3) ? 'selected' : ''}} value="3">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-0 case-0-1" style="{{ (isset($case1['value']) and $case1['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][answer][no_contact][1][contact_name]" value="{{ (isset($case1[1]['contact_name']) and $case1[1]['contact_name'] != '') ? $case1[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][answer][no_contact][1][task_text]" value="{{ (isset($case1[1]['task_text']) and $case1[1]['task_text'] != '') ? $case1[1]['task_text'] : 'Заполнить карточку клиента' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="case-0 case-0-2" style="{{ (isset($case1['value']) and $case1['value']==2) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][answer][no_contact][2][contact_name]" value="{{ (isset($case1[2]['contact_name']) and $case1[2]['contact_name'] != '') ? $case1[2]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][answer][no_contact][2][task_text]" value="{{ (isset($case1[2]['task_text']) and $case1[2]['task_text'] != '') ? $case1[2]['task_text'] : 'Заполнить карточку сделки' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Название создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][answer][no_contact][2][lead_name]" value="{{ (isset($case1[2]['lead_name']) and $case1[2]['lead_name'] != '') ? $case1[2]['lead_name'] : 'Входящий звонок' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Статус создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="case[input][answer][no_contact][2][lead_status]" class="form-control">
                                                                    <option>Выберите статус</option>
                                                                    @foreach($leads_statuses as $pipeline)
                                                                        <option disabled="">{{$pipeline['name']}}</option>
                                                                        @foreach($pipeline['statuses'] as $lead_status)
                                                                            <option value="{{$lead_status['id']}}" {{ (isset($case1[2]['lead_status']) and $case1[2]['lead_status'] == $lead_status['id']) ? 'selected' : '' }} >{{$lead_status['name']}}</option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3>Не отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                            <?php $case2 = isset($cases['input']['no_answer']['contact_exist']['task_text']) ? $cases['input']['no_answer']['contact_exist']['task_text'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            Создание в существующий контакт задачи
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-1 col-md-offset-1">
                                                            Текст задачи:
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="case[input][no_answer][contact_exist][task_text]" value="{{ (isset($case2) and $case2 != '') ? $case2 : 'Перезвонить клиенту' }}" />
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php $case3 = isset($cases['input']['no_answer']['no_contact']) ? $cases['input']['no_answer']['no_contact'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[input][no_answer][no_contact][value]">
                                                                <option {{(isset($case3['value']) and $case3['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case3['value']) and $case3['value']==2) ? 'selected' : ''}} value="2">Создание контакта, сделки и задачи в ней</option>
                                                                <option {{(isset($case3['value']) and $case3['value']==3) ? 'selected' : ''}} value="3">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-1 case-1-1" style="{{ (isset($case3['value']) and $case3['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][no_answer][no_contact][1][contact_name]" value="{{ (isset($case3[1]['contact_name']) and $case3[1]['contact_name'] != '') ? $case3[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][no_answer][no_contact][1][task_text]" value="{{ (isset($case3[1]['task_text']) and $case3[1]['task_text'] != '') ? $case3[1]['task_text'] : 'Перезвонить' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="case-1 case-1-2" style="{{ (isset($case3['value']) and $case3['value']==2) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer case-0-2">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][no_answer][no_contact][2][contact_name]" value="{{ (isset($case3[2]['contact_name']) and $case3[2]['contact_name'] != '') ? $case3[2]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][no_answer][no_contact][2][task_text]" value="{{ (isset($case3[2]['task_text']) and $case3[2]['task_text'] != '') ? $case3[2]['task_text'] : 'Перезвонить' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Название создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[input][no_answer][no_contact][2][lead_name]" value="{{ (isset($case3[2]['lead_name']) and $case3[2]['lead_name'] != '') ? $case3[2]['lead_name'] : 'Попущеный звонок' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Статус создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="case[input][no_answer][no_contact][2][lead_status]" class="form-control">
                                                                    <option>Выберите статус</option>
                                                                    @foreach($leads_statuses as $pipeline)
                                                                        <option disabled="">{{$pipeline['name']}}</option>
                                                                        @foreach($pipeline['statuses'] as $lead_status)
                                                                            <option value="{{$lead_status['id']}}" {{ (isset($case3[2]['lead_status']) and $case3[2]['lead_status'] == $lead_status['id']) ? 'selected' : '' }} >{{$lead_status['name']}}</option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="output" class="tab-pane fade">
                                                <h3>Отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                        </div>
                                                        <div class="col-md-6">
                                                            Добавление в существующий контакт записи разговора
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php $case4 = isset($cases['output']['answer']['no_contact']) ? $cases['output']['answer']['no_contact'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[output][answer][no_contact][value]">
                                                                <option {{(isset($case4['value']) and $case4['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case4['value']) and $case4['value']==2) ? 'selected' : ''}} value="2">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-2 case-2-1" style="{{ (isset($case4['value']) and $case4['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[output][answer][no_contact][1][contact_name]" value="{{ (isset($case4[1]['contact_name']) and $case4[1]['contact_name'] != '') ? $case4[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[output][answer][no_contact][1][task_text]" value="{{ (isset($case4[1]['task_text']) and $case4[1]['task_text'] != '') ? $case4[1]['task_text'] : 'Заполнить карточку клиента' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h3>Не отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                        </div>
                                                        <div class="col-md-6">
                                                            Ничего не делаем
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php $case5 = isset($cases['output']['no_answer']['no_contact']) ? $cases['output']['no_answer']['no_contact'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[output][no_answer][no_contact][value]">
                                                                <option {{(isset($case5['value']) and $case5['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case5['value']) and $case5['value']==2) ? 'selected' : ''}} value="2">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-3 case-3-1" style="{{ (isset($case5['value']) and $case5['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[output][no_answer][no_contact][1][contact_name]" value="{{ (isset($case5[1]['contact_name']) and $case5[1]['contact_name'] != '') ? $case5[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[output][no_answer][no_contact][1][task_text]" value="{{ (isset($case5[1]['task_text']) and $case5[1]['task_text'] != '') ? $case5[1]['task_text'] : 'Перезвонить' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="getcall" class="tab-pane fade">
                                                <h3>Отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                        </div>
                                                        <div class="col-md-6">
                                                            Добавление в существующий контакт записи разговора
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php $case4 = isset($cases['getcall']['answer']['no_contact']) ? $cases['getcall']['answer']['no_contact'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[getcall][answer][no_contact][value]">
                                                                <option {{(isset($case4['value']) and $case4['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case4['value']) and $case4['value']==2) ? 'selected' : ''}} value="2">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-4 case-4-1" style="{{ (isset($case4['value']) and $case4['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[getcall][answer][no_contact][1][contact_name]" value="{{ (isset($case4[1]['contact_name']) and $case4[1]['contact_name'] != '') ? $case4[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[getcall][answer][no_contact][1][task_text]" value="{{ (isset($case4[1]['task_text']) and $case4[1]['task_text'] != '') ? $case4[1]['task_text'] : 'Заполнить карточку клиента' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h3>Не отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                        </div>
                                                        <div class="col-md-6">
                                                            Ничего не делаем
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php $case5 = isset($cases['getcall']['no_answer']['no_contact']) ? $cases['getcall']['no_answer']['no_contact'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[getcall][no_answer][no_contact][value]">
                                                                <option {{(isset($case5['value']) and $case5['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case5['value']) and $case5['value']==2) ? 'selected' : ''}} value="2">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-5 case-5-1" style="{{ (isset($case5['value']) and $case5['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[getcall][no_answer][no_contact][1][contact_name]" value="{{ (isset($case5[1]['contact_name']) and $case5[1]['contact_name'] != '') ? $case5[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[getcall][no_answer][no_contact][1][task_text]" value="{{ (isset($case5[1]['task_text']) and $case5[1]['task_text'] != '') ? $case5[1]['task_text'] : 'Перезвонить' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="calltracking" class="tab-pane fade">
                                                <h3>Отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                        </div>
                                                        <div class="col-md-4">
                                                            Добавление в существующий контакт записи разговора
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php
                                                            $case1 = isset($cases['calltracking']['answer']['no_contact']) ? $cases['calltracking']['answer']['no_contact'] : '';
                                                            ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[calltracking][answer][no_contact][value]">
                                                                <option {{(isset($case1['value']) and $case1['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case1['value']) and $case1['value']==2) ? 'selected' : ''}} value="2">Создание контакта, сделки и задачи в ней</option>
                                                                <option {{(isset($case1['value']) and $case1['value']==3) ? 'selected' : ''}} value="3">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-ct case-ct-1" style="{{ (isset($case1['value']) and $case1['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][answer][no_contact][1][contact_name]" value="{{ (isset($case1[1]['contact_name']) and $case1[1]['contact_name'] != '') ? $case1[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][answer][no_contact][1][task_text]" value="{{ (isset($case1[1]['task_text']) and $case1[1]['task_text'] != '') ? $case1[1]['task_text'] : 'Заполнить карточку клиента' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="case-ct case-ct-2" style="{{ (isset($case1['value']) and $case1['value']==2) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][answer][no_contact][2][contact_name]" value="{{ (isset($case1[2]['contact_name']) and $case1[2]['contact_name'] != '') ? $case1[2]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][answer][no_contact][2][task_text]" value="{{ (isset($case1[2]['task_text']) and $case1[2]['task_text'] != '') ? $case1[2]['task_text'] : 'Заполнить карточку сделки' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Название создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][answer][no_contact][2][lead_name]" value="{{ (isset($case1[2]['lead_name']) and $case1[2]['lead_name'] != '') ? $case1[2]['lead_name'] : 'Входящий звонок' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Статус создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="case[calltracking][answer][no_contact][2][lead_status]" class="form-control">
                                                                    <option>Выберите статус</option>
                                                                    @foreach($leads_statuses as $pipeline)
                                                                        <option disabled="">{{$pipeline['name']}}</option>
                                                                        @foreach($pipeline['statuses'] as $lead_status)
                                                                            <option value="{{$lead_status['id']}}" {{ (isset($case1[2]['lead_status']) and $case1[2]['lead_status'] == $lead_status['id']) ? 'selected' : '' }} >{{$lead_status['name']}}</option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3>Не отвечен</h3>
                                                <div class="col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <b>Контакт существует:</b>
                                                            <?php $case2 = isset($cases['calltracking']['no_answer']['contact_exist']['task_text']) ? $cases['calltracking']['no_answer']['contact_exist']['task_text'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            Создание в существующий контакт задачи
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-1 col-md-offset-1">
                                                            Текст задачи:
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="case[calltracking][no_answer][contact_exist][task_text]" value="{{ (isset($case2) and $case2 != '') ? $case2 : 'Перезвонить клиенту' }}" />
                                                        </div>
                                                    </div>
                                                    <div class="row top_buffer">
                                                        <div class="col-md-2">
                                                            <b>Контакт отсутствует:</b>
                                                            <?php $case3 = isset($cases['calltracking']['no_answer']['no_contact']) ? $cases['calltracking']['no_answer']['no_contact'] : ''; ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="case[calltracking][no_answer][no_contact][value]">
                                                                <option {{(isset($case3['value']) and $case3['value']==1) ? 'selected' : ''}} value="1">Создание контакта и задачу в нем</option>
                                                                <option {{(isset($case3['value']) and $case3['value']==2) ? 'selected' : ''}} value="2">Создание контакта, сделки и задачи в ней</option>
                                                                <option {{(isset($case3['value']) and $case3['value']==3) ? 'selected' : ''}} value="3">Ничего не делаем</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="case-ct1 case-ct1-1" style="{{ (isset($case3['value']) and $case3['value']==1) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][no_answer][no_contact][1][contact_name]" value="{{ (isset($case3[1]['contact_name']) and $case3[1]['contact_name'] != '') ? $case3[1]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][no_answer][no_contact][1][task_text]" value="{{ (isset($case3[1]['task_text']) and $case3[1]['task_text'] != '') ? $case3[1]['task_text'] : 'Перезвонить' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="case-ct1 case-ct1-2" style="{{ (isset($case3['value']) and $case3['value']==2) ? '' : 'display:none;'  }}">
                                                        <div class="row top_buffer case-ct0-2">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Имя создаваемого контакта:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][no_answer][no_contact][2][contact_name]" value="{{ (isset($case3[2]['contact_name']) and $case3[2]['contact_name'] != '') ? $case3[2]['contact_name'] : 'Автоконтакт' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Текст создаваемой задачи:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][no_answer][no_contact][2][task_text]" value="{{ (isset($case3[2]['task_text']) and $case3[2]['task_text'] != '') ? $case3[2]['task_text'] : 'Перезвонить' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Название создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="case[calltracking][no_answer][no_contact][2][lead_name]" value="{{ (isset($case3[2]['lead_name']) and $case3[2]['lead_name'] != '') ? $case3[2]['lead_name'] : 'Попущеный звонок' }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row top_buffer">
                                                            <div class="col-md-1 col-md-offset-1">
                                                                Статус создаваемой сделки:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="case[calltracking][no_answer][no_contact][2][lead_status]" class="form-control">
                                                                    <option>Выберите статус</option>
                                                                    @foreach($leads_statuses as $pipeline)
                                                                        <option disabled="">{{$pipeline['name']}}</option>
                                                                        @foreach($pipeline['statuses'] as $lead_status)
                                                                            <option value="{{$lead_status['id']}}" {{ (isset($case3[2]['lead_status']) and $case3[2]['lead_status'] == $lead_status['id']) ? 'selected' : '' }} >{{$lead_status['name']}}</option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="panel">
                            <header class="panel-heading">
                                UTM метки и менеджера
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3>Номера телефонов</h3>
                                        <?php $phones = unserialize($binotel->phones);
                                        ?>
                                        <table class="table">
                                            <thead>
                                            <th>№ телефона в Бинотел</th>
                                            <th>Менеджер в CRM</th>
                                            </thead>
                                            <tbody>
                                            @if( isset($phones) and is_array($phones) )
                                                @foreach($phones as $phone => $employer)
                                                    <tr>
                                                        <td>
                                                            <input class="form-control" required type="number" name="phones[phone][]" value="{{$phone}}">
                                                        </td>
                                                        <td>
                                                            <select required name="phones[manager][]" class="form-control">
                                                                <option value="">Выберите менеджера</option>
                                                                @foreach($managers as $manager)
                                                                    <option {{$employer == $manager['id'] ? 'selected' : ''}} value="{{$manager['id']}}">{{$manager['name']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-danger del_phone" type="button"><i class="fa fa-trash-o"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td>
                                                        <input class="form-control" required type="number" name="phones[phone][]" value="">
                                                    </td>
                                                    <td>
                                                        <select required name="phones[manager][]" class="form-control">
                                                            <option value="">Выберите менеджера</option>
                                                            @foreach($managers as $manager)
                                                                <option value="{{$manager['id']}}">{{$manager['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-danger del_phone" type="button"><i class="fa fa-trash-o"></i></button>
                                                    </td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-default add_phone">Добавить</button>
                                    </div>
                                    <div class="col-md-6 col-md-offset-1">
                                        <h3>UTM метки</h3>
                                        <?php
                                        $utm_target = unserialize($binotel->utm_target);
                                        ?>
                                        <div class="utm_step_1">
                                            <labe>Во что писать</labe>
                                            <select class="form-control" name="utm_target[entity]">
                                                <option {{ (isset($utm_target['entity']) and $utm_target['entity'] == '0') ? 'selected' :  ''  }} value="0">В никуда</option>
                                                <option {{ (isset($utm_target['entity']) and $utm_target['entity'] == 'leads') ? 'selected' :  ''  }} value="leads">В сделку</option>
                                                <option {{ (isset($utm_target['entity']) and $utm_target['entity'] == 'contacts') ? 'selected' :  ''  }} value="contacts">В контакт</option>
                                            </select>
                                        </div>

                                        <div class="utm_step_2 hidden">
                                            <labe>Куда писать</labe>
                                            <select class="form-control" name="utm_target[place]">
                                                <option {{ (isset($utm_target['place']) and $utm_target['place'] == 'notice') ? 'selected' :  ''  }} value="notice">В примечания</option>
                                                <option {{ (isset($utm_target['place']) and $utm_target['place'] == 'fields') ? 'selected' :  ''  }} value="fields">В поля</option>
                                            </select>
                                        </div>
                                        <div class="utm_step_3 hidden">
                                            <table class="table table-striped">
                                                <thead>
                                                <th>Метка</th>
                                                <th>Поле в CRM</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>gaClientId:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[gaClientId]">
                                                            <?php echo isset($options['gaClientId']) ? $options['gaClientId'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>utm_source:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[utm_source]">
                                                            <?php echo isset($options['utm_source']) ? $options['utm_source'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>utm_medium:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[utm_medium]">
                                                            <?php echo isset($options['utm_medium']) ? $options['utm_medium'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>utm_campaign:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[utm_campaign]">
                                                            <?php echo isset($options['utm_campaign']) ? $options['utm_campaign'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>utm_content:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[utm_content]">
                                                            <?php echo isset($options['utm_content']) ? $options['utm_content'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>utm_term:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[utm_term]">
                                                            <?php echo isset($options['utm_term']) ? $options['utm_term'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>ipAddress:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[ipAddress]">
                                                            <?php echo isset($options['ipAddress']) ? $options['ipAddress'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>domain:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[domain]">
                                                            <?php echo isset($options['domain']) ? $options['domain'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>gaTrackingId:</td>
                                                    <td>
                                                        <select class="form-control" name="utm[gaTrackingId]">
                                                            <?php echo isset($options['gaTrackingId']) ? $options['gaTrackingId'] :  $options; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->
@endsection