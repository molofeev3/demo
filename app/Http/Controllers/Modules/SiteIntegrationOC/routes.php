<?php
/**
 * Created by PhpStorm.
 * User: molofeev
 * Date: 17.05.17
 * Time: 15:56
 */
Route::group(['namespace' => 'Modules\SiteIntegrationOC\Controllers'], function()
{
    Route::get('/site_integration_oc', ['uses'=>'SiteIntegrationOCController@index']);
    Route::get('/site_integration_oc/{id}', ['uses'=>'SiteIntegrationOCController@site'])->where(['id'=>'[0-9]+']);
    Route::get('/site_integration_oc/add', ['uses'=>'SiteIntegrationOCController@add_site']);
    Route::post('/site_integration_oc/add_new_site', ['uses'=>'SiteIntegrationOCController@add_new_site']);
    Route::get('/site_integration_oc/del_site/{id}', ['uses'=>'SiteIntegrationOCController@del_site'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration_oc/change_name/{id}' , ['uses'=>'SiteIntegrationOCController@change_name']);
    Route::post('/site_integration_oc/save/{id}', ['uses'=>'SiteIntegrationOCController@save'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration_oc/employers/disactive/{id}', ['uses'=>'SiteIntegrationOCController@getDisactiveEmployers'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration_oc/employers/active/{id}', ['uses'=>'SiteIntegrationOCController@getActiveEmployers'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration_oc/employers/all/{id}', ['uses'=>'SiteIntegrationOCController@getAllEmployers'])->where(['id'=>'[0-9]+']);

    Route::get('/site_integration_oc/{id}/log', ['uses'=>'SiteIntegrationOCController@getLog'])->where(['id'=>'[0-9]+']);
});
Route::get('/amo/integration_oc/{id}', array('middleware' => 'guest', function($id, \Illuminate\Http\Request $request) {
        return App\Http\Controllers\Modules\SiteIntegrationOC\Controllers\Amo::integrate($id, $request);
//    return 'Hello';
}));