<?php

namespace App\Http\Controllers\Modules\SiteIntegrationOC\Controllers; // define namespace


use App\Http\Controllers\Controller;
use App\Menu;
use App\Module;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema; // use base controller class


class MainController extends Controller // example of main controller for install and uninstall module
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function install()
    {
        Schema::create('site_integration_oc', function(Blueprint $table) // example of creating DB column
        {
            $table->increments('id');
            //site
            $table->string('url');
            $table->string('short_name');
            //config
            $table->string('contact_name')->nullable();
            $table->string('contact_auto_name')->nullable();
            $table->longText('contact_fields')->nullable();
            $table->string('lead_name')->nullable();
            $table->string('lead_auto_name')->nullable();
            $table->integer('lead_status')->nullable();
            $table->longText('lead_fields')->nullable();
            $table->integer('task_type')->nullable();
            $table->longText('task_comment')->nullable();
            $table->integer('task_until')->nullable();
            $table->integer('dublicate_controll')->nullable();
            $table->integer('distribution_rule')->nullable();
            $table->longText('schedule')->nullable();
            $table->longText('distributions')->nullable();
            $table->longText('utm_terms');
            $table->string('tasks');
            $table->string('lead_price');
            $table->integer('user_id');
            $table->longText('log');
            $table->timestamps();
        });

        $menu_item = new Menu(); // example of add module to menu
        $menu_item->title = 'Интеграция с OC';
        $menu_item->url = 'site_integration_oc';
        $menu_item->parent = 3; //subitem for Modules item, subitems hasnt ico
        $menu_item->save();

        $module = new Module();// example of registration module
        $module->name = 'SiteIntegrationOC';
        $module->save();

        return redirect('/modules');
    }

    public function uninstall(){
        Schema::drop('site_integration_oc');

        Menu::where('title', '=', 'Интеграция с OC')->delete(); //example of removing module from menu

        Module::where('name', '=', 'SiteIntegrationOC')->delete(); // example of uninstaling module

        return redirect('/modules');
    }
}
