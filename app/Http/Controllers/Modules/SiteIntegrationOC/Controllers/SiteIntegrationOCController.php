<?php

namespace App\Http\Controllers\Modules\SiteIntegrationOC\Controllers; // define namespace


use App\Http\Controllers\Controller; // use base controller class
use App\Http\Controllers\Modules\SiteIntegrationOC\SiteIntegrationOC;
use Illuminate\Support\Facades\Auth; // including Model class


class SiteIntegrationOCController extends Controller
{


    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->middleware('auth');
        $this->middleware('amo');
        $this->request = $request;
    }

    public function index(){
        $user = Auth::user();
        $sites = SiteIntegrationOC::all();
        return view('SiteIntegrationOC.Views.index', array('user' => $user, 'sites'=>$sites));
    }

    public function getLog($id){
        $user = Auth::user();
        $site = SiteIntegrationOC::find($id);
        $log = $site->log;
        return view('SiteIntegrationOC.Views.log', array('user' => $user,'log'=>$log));
    }

    public function site($id)
    {
        $user = Auth::user();
        $amo = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain);
        $site = SiteIntegrationOC::find($id);
        if(!isset($site)){
            return view('errors.fail', ['errormsg' => 'Нету такого сайта, или Вы жульничаете', 'link' => '/']);
        }
        
        $html = new \Htmldom($site->url);
        //$forms = $html->find('form');
        $site_fields = array();
        //foreach ($forms as $form) {
            $inputs = $html->find('input');
            foreach ($inputs as $input) {
                $site_fields[$input->name] = (isset($input->placeholder) and $input->placeholder != '') ? $input->placeholder : $input->name;
            }
            $textareas = $html->find('textarea');
            foreach ($textareas as $textarea) {
                $site_fields[$textarea->name] = (isset($textarea->placeholder) and $textarea->placeholder != '') ? $textarea->placeholder : $textarea->name;
            }
            $selects = $html->find('select');
            foreach ($selects as $select) {
                $site_fields[$select->name] = (isset($select->placeholder) and $select->placeholder != '') ? $select->placeholder : $select->name;
            }
       // }

        if(!isset($site_fields) or empty($site_fields)){
            $site->delete();
            return view('errors.fail', ['errormsg' => 'Не верно указан адресс сайта или на сайте нету полей. Пересоздайте сайт заново.', 'link' => '/integration_module/']);
        }
        $amo_acc_data = $amo->getAccountInfo()['account'];

        $validations = $this->validate_config($site);

        $data = array(
            'site' => $site->toArray(),
            'custom_fields' => $amo_acc_data['custom_fields'],
            'site_fields' => $site_fields,
            'employers' => $amo_acc_data['users'],
            'leads_statuses' => $amo_acc_data['pipelines'],
            'task_types' => $amo_acc_data['task_types'],
            'configuration' => $site,
            'validations' => $validations,
            'user' => $user
        );
        return view('SiteIntegrationOC.Views.site_custom', $data); #todo change view
    }

    public function getActiveEmployers($id){
        $site = SiteIntegrationOC::find($id);
        $schedule = unserialize($site->schedule);
        $result = array();
        foreach ($schedule as $id => $employer){
            if(isset($employer['active'])){
                $result[] = $id;
            }
        }
        return response()->json(['schedule' => $result]);
    }

    public function getDisactiveEmployers($id){
        $site = SiteIntegrationOC::find($id);
        $schedule = unserialize($site->schedule);
        $result = array();
        foreach ($schedule as $id => $employer){
            if(!isset($employer['active'])){
                $result[] = $id;
            }
        }
        return response()->json(['schedule' => $result]);
    }

    public function getAllEmployers($id){
        $site = SiteIntegrationOC::find($id);
        $schedule = unserialize($site->schedule);
        $result = array();
        foreach ($schedule as $id => $employer){
            $result[] = $id;
        }
        return response()->json(['schedule' => $result]);
    }

    private function validate_config($configuration){
        $validations = array();
        if($configuration['contact_name'] == '' and $configuration['contact_auto_name'] == ''){
            $validations[] = 'Выберите имя контакта или укажите автоимя';
        }
        if(empty(unserialize($configuration['contact_fields']))){
            $validations[] = 'Укажите поля для данных контакта';
        }
        if($configuration['lead_name'] == '' and $configuration['lead_auto_name'] == ''){
            $validations[] = 'Выберите имя сделки или укажите автоимя';
        }
        if($configuration['lead_status'] == 0){
            $validations[] = 'Укажите статус сделки';
        }
        if($configuration['task_type'] == 0){
            $validations[] = 'Укажите тип задачи';
        }
        if($configuration['task_comment'] == '' and $configuration['tasks'] == 'on'){
            $validations[] = 'Укажите описание задачи';
        }
        if($configuration['task_until'] == 0){
            $validations[] = 'Укажите сроки выполнеия задачи';
        }
        if($configuration['dublicate_controll'] == 0){
            $validations[] = 'Укажите правило для контроля дублей';
        }
        if($configuration['distribution_rule'] == 0){
            $validations[] = 'Выберите правило распределения сделок';
        }

        return $validations;
    }

    public function add_new_site()
    {
        $input = $this->request->all();
        $this->validate($this->request, [
            'short_name' => 'required',
            'url' => 'required|url|unique:site_integration_oc,url'
        ]);

        $file_headers = @get_headers($input['url']);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return view('errors.fail', ['errormsg' => 'URL не действителен или сайт недоступен.', 'link' => '/site_integration_oc/']);
        }

        $user = Auth::user();
        $site = new SiteIntegrationOC([
            'short_name' => $input['short_name'],
            'url' => $input['url'],
        ]);
        $site->save();
        return redirect('/site_integration_oc/'.$site->id);
    }

    public function add_site(){
        $user = Auth::user();
        return view('SiteIntegrationOC.Views.add_site', ['user' => $user]);
    }

    public function del_site($id)
    {
        $site = SiteIntegrationOC::find($id);
        if(!isset($site)){
            return view('errors.fail', ['errormsg' => 'Нету такого сайта, или Вы жульничаете', 'link' => '/site_integration_oc/']);
        }
        $site->delete();

        return redirect('/site_integration_oc/');
    }

    public function change_name($id){
        $input = $this->request->all();
        $site = SiteIntegrationOC::find($id);
        $site->short_name = $input['new_name'];
        $site->save();
        return $this->index($id);
    }

    public function save($id)
    {
        $input = $this->request->all();
        $contact_fields = array();
        $lead_fields = array();

        if(isset($input['contact_field_site'])){
            foreach ($input['contact_field_site'] as $key => $value) {
                if($input['contact_field_amo'][$key] != '' and $value != ''){
                    $contact_fields[$input['contact_field_amo'][$key]] = $value;
                }
            }
        }

        if(isset($input['lead_fields_site'])){
            foreach ($input['lead_fields_site'] as $key => $value) {
                if($input['lead_fields_amo'][$key] != '' and $value != ''){
                    $lead_fields[$input['lead_fields_amo'][$key]] = $value;
                }
            }
        }
        $schedule = array();
        if (isset($input['schedule'])) {
            $schedule = $input['schedule'];
        }
        $schedule = serialize($schedule);

        $site = SiteIntegrationOC::find($id);
        $site->contact_name = (isset($input['contact_name'])) ? $input['contact_name'] : '';
        $site->contact_auto_name = $input['contact_auto_name'];
        $site->contact_fields = serialize($contact_fields);
        $site->lead_name = (isset($input['lead_name'])) ? $input['lead_name'] : '';
        $site->lead_auto_name = $input['lead_auto_name'];
        $site->lead_status = $input['lead_status'];
        $site->lead_fields = serialize($lead_fields);
        $site->task_type = $input['task_type'];
        $site->task_comment = $input['task_comment'];
        $site->task_until = $input['task_until'];
        $site->dublicate_controll = $input['dublicate_controll'];
        $site->distribution_rule = $input['distribution_rule'];
        $site->schedule = $schedule;
        $site->utm_terms = serialize($input['utm_terms']);
        $site->tasks = (isset($input['tasks'])) ? $input['tasks'] : 'off';
        $site->lead_price = (isset($input['lead_price'])) ? $input['lead_price'] : '';
        $site->user_id = Auth::user()->id;
        $site->save();
        return redirect('/site_integration_oc/'.$id);
    }

}