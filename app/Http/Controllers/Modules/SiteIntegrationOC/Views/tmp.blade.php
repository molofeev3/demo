@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h1 class="page-header">Заявки с сайта <button type="button" style="border: 0;background: none;" data-toggle="modal" data-target="#myModal">"{{$site['short_name']}}"</button></h1>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Настройки
                        </header>
                        <div class="panel-body">
<form method="POST" action="{{ url('/site_integration_oc/save/'.$site['id']) }}">
                                {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
                            <div class="schedule_table">
                                <table>
                                    <tr>
                                        <td>Пн.</td>
                                        <td><input type="checkbox" class="check_row checkbox"></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <label class="label_check" for="checkbox-0{{$i}}">
                                                    <input name="shchedule[employer_id][1][]" id="checkbox-0{{$i}}" value="{{  ($i<10) ? '0'.$i : $i  }}:00-{{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}:00" type="checkbox" />
                                                </label>
                                            </td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>Вт.</td>
                                        <td><input type="checkbox" class="check_row checkbox"></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <label class="label_check" for="checkbox-1{{$i}}">
                                                    <input name="shchedule[employer_id][2][]" id="checkbox-0{{$i}}" value="{{  ($i<10) ? '0'.$i : $i  }}:00-{{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}:00" type="checkbox" />
                                                </label>
                                            </td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>Ср.</td>
                                        <td><input type="checkbox" class="check_row checkbox"></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <label class="label_check" for="checkbox-2{{$i}}">
                                                    <input name="shchedule[employer_id][3][]" id="checkbox-0{{$i}}" value="{{  ($i<10) ? '0'.$i : $i  }}:00-{{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}:00" type="checkbox" />
                                                </label>
                                            </td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>Чт.</td>
                                        <td><input type="checkbox" class="check_row checkbox"></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <label class="label_check" for="checkbox-3{{$i}}">
                                                    <input name="shchedule[employer_id][4][]" id="checkbox-0{{$i}}" value="{{  ($i<10) ? '0'.$i : $i  }}:00-{{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}:00" type="checkbox" />
                                                </label>
                                            </td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>Пт.</td>
                                        <td><input type="checkbox" class="check_row checkbox"></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <label class="label_check" for="checkbox-4{{$i}}">
                                                    <input name="shchedule[employer_id][5][]" id="checkbox-0{{$i}}" value="{{  ($i<10) ? '0'.$i : $i  }}:00-{{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}:00" type="checkbox" />
                                                </label>
                                            </td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>Сб.</td>
                                        <td><input type="checkbox" class="check_row checkbox"></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <label class="label_check" for="checkbox-5{{$i}}">
                                                    <input name="shchedule[employer_id][6][]" id="checkbox-0{{$i}}" value="{{  ($i<10) ? '0'.$i : $i  }}:00-{{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}:00" type="checkbox" />
                                                </label>
                                            </td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td>Вс.</td>
                                        <td><input type="checkbox" class="check_row checkbox"></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <label class="label_check" for="checkbox-6{{$i}}">
                                                    <input name="shchedule[employer_id][7][]" id="checkbox-0{{$i}}" value="{{  ($i<10) ? '0'.$i : $i  }}:00-{{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}:00" type="checkbox" />
                                                </label>
                                            </td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        @for($i = 0; $i <= 23; $i++)
                                            <td>
                                                <input type="checkbox" class="check_col checkbox">
                                                {{  ($i<10) ? '0'.$i : $i  }}
                                                <br>
                                                {{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}
                                            </td>
                                        @endfor
                                    </tr>
                                </table>
                                <button type="button" class="btn btn-xs btn-default work8">Рабочее время с 8:00</button>
                                <button type="button" class="btn btn-xs btn-default work9">Рабочее время с 9:00</button>
                                <button type="button" class="btn btn-xs btn-default clear_all">Снять все</button>
                            </div>
</form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->
@endsection
