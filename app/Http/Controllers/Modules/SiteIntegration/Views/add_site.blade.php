@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-5">
                    <section class="panel">
                        <header class="panel-heading">
                            Добавить новый сайт
                        </header>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/site_integration/add_new_site') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                        <label for="url" class="col-md-3 control-label">URL Aдресс</label>
                        <div class="col-md-9">
                            <input id="url" type="text" class="form-control" name="url" value="{{ old('url') }}">
                            @if ($errors->has('url'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('url') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('short_name') ? ' has-error' : '' }}">
                        <label for="short_name" class="col-md-3 control-label">Краткое название</label>
                        <div class="col-md-9">
                            <input id="short_name" type="text" class="form-control" name="short_name" value="{{ old('short_name') }}">
                            @if ($errors->has('short_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('short_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">
                                Добавить
                            </button>
                        </div>
                    </div>

                </form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->
@endsection
