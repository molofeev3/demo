@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <div class="row top_buffer">
                <div class="col-md-3">
                    <h1>Ваши сайты</h1>
                </div>
                <div class="col-md-2 col-md-offset-7 top_buffer">
                    @if(!Auth::user()->is_viewer())
                    <a href="/site_integration/add" class="btn btn-primary">Добавить сайт</a>
                    @endif
                </div>
            </div>

            <div class="row top_buffer">
                <div class="col-md-12">
                    <section class="panel">

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>URL</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($sites) != 0)
                                    @foreach($sites as $key=>$site)
                                        <tr>
                                            <td>{{$site->short_name}}</td>
                                            <td>{{$site->url}}</td>
                                            <td>
                                                <a href="{{ url('/site_integration/'.$site->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                                @if(!Auth::user()->is_viewer())
                                                <a href="{{ url('/site_integration/del_site/'.$site->id) }}" data-confirm="true" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                                    <a href="{{ url('/site_integration/'.$site->id.'/log')  }}" class="btn btn-success btn-xs"><i class="fa fa-eye "></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">
                                            У вас пока нету ниодного созданого сайта
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->
@endsection
