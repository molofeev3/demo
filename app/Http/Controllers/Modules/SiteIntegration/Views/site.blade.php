@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h1 class="page-header site-name">Заявки с сайта "{{$site['short_name']}}"@if(!Auth::user()->is_viewer())<button type="button" class="btn btn-white site-name-pensil" data-toggle="modal" data-target="#myModal"><span class="fa fa-pencil"></span></button>@endif</h1>
            <form class="site-data-form" method="POST" action="{{ url('/site_integration/save/'.$site['id']) }}">
                {{ csrf_field() }}
                @if(!Auth::user()->is_viewer())
                <a class="del-site-link btn btn-danger pull-right" href="{{ url('/site_integration/del_site/'.$site['id']) }}" data-confirm="true"  style="position: absolute; right: 7%; top: 12%;">Удалить сайт</a>
                <button type="submit" class="btn btn-primary" style="position: absolute; right: 1%; top: 12%;">Сохранить</button>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Сорудники
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Правило распределения заявок между менеджерами<span class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Если по расписанию в момент поступления заявки за нее отвечают более одного менеджера то необходимо распределять заявки между ними, сделать это можна следующими способами: 1) По очередно- заявки раздаются всем активным менеджерам по кругу. 2) По весам (с учетом заданного веса). Долджна быть возможность прописать менеджеру вес к примеру 1 или 2. соответственно менеджео с весом в 2 будет получать в раза больше заявок чем менеджер с весом 1.3) Кто первый - заявка попадает на администратора, а потом раздается менеджерам у которых нет задач в статусе Новая заявка.4) По количеству открытых сделок - заявки попадают менеджеру у которго меньше всего открытых сделок."></span></h3>
                                        <select class="form-control" name="distribution_rule">
                                            <option <?php echo ($configuration['distribution_rule'] == 1) ? 'selected' : ''; ?> value="1">По очередно</option>
                                            <option <?php echo ($configuration['distribution_rule'] == 2) ? 'selected' : ''; ?> value="2">По весам</option>
                                            <option <?php echo ($configuration['distribution_rule'] == 3) ? 'selected' : ''; ?> value="3">Кто первый</option>
                                            <option <?php echo ($configuration['distribution_rule'] == 4) ? 'selected' : ''; ?> value="4">По количеству открытых сделок</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>График приема заявок</h3>
                                        <div class="employers-statuses-btns">
                                            <button type="button" class="btn btn-xs btn-white active-employers">Акивные</button>
                                            <button type="button" class="btn btn-xs btn-default disactive-employers">Неактивные</button>
                                            <button type="button" class="btn btn-xs btn-default all-employers">Все</button>
                                        </div>
                                        <br>
                                        <?php
                                        $schedule = unserialize($configuration['schedule']);
                                        ?>
                                        <select class="js-example-basic-multiple" multiple="multiple">
                                            @foreach($employers as $employer)
                                                <option {{(isset($schedule[$employer['id']]['active']) and $schedule[$employer['id']]['active']=='on') ? 'selected' : ''}} value="{{$employer['id']}}">{{$employer['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="employer-list">
                            @foreach($employers as $employer)
                                <section class="panel employer_panel">
                                    <header class="panel-heading">
                                        {{$employer['name']}}
                                    </header>
                                    <div class="panel-body">
                                        <div class="schedule_table" id="employer-{{$employer['id']}}">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Статус:</label>
                                                    <input type="checkbox" class="js-switch" name="schedule[{{$employer['id']}}][active]" <?php echo (isset($schedule[$employer['id']]['active']) and $schedule[$employer['id']]['active']=='on') ? 'checked' : '' ; ?>>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>
                                                        Вес:
                                                    </label>
                                                    <select class="" name="schedule[{{$employer['id']}}][skill]">
                                                        <option <?php echo (isset($schedule[$employer['id']]['skill']) and $schedule[$employer['id']]['skill']=='1') ? 'selected' : '' ; ?> value="1">1</option>
                                                        <option <?php echo (isset($schedule[$employer['id']]['skill']) and $schedule[$employer['id']]['skill']=='2') ? 'selected' : '' ; ?> value="2">2</option>
                                                        <option <?php echo (isset($schedule[$employer['id']]['skill']) and $schedule[$employer['id']]['skill']=='3') ? 'selected' : '' ; ?> value="3">3</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row top_buffer">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-xs btn-default work8">Рабочее время с 8:00</button>
                                                    <button type="button" class="btn btn-xs btn-default work9">Рабочее время с 9:00</button>
                                                    <button type="button" class="btn btn-xs btn-default clear_all">Снять все</button>
                                                    <button type="button" class="btn btn-xs btn-default check_all">Выбрать все</button>
                                                    <table class="top_buffer">
                                                        <tr>
                                                            <td>Пн.</td>
                                                            <td><input type="checkbox" class="check_row checkbox"></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <label class="label_check" for="{{$employer['id']}}checkbox-0{{$i}}">
                                                                        <?php
                                                                        $start_time = ($i<10) ? '0'.$i : $i;
                                                                        $end_time  = (($i+1)<10) ? '0'.($i+1) : ($i+1);
                                                                        $time = $start_time.':00-'.$end_time.':00';
                                                                        $cell = isset($schedule[$employer['id']][1]) ? $schedule[$employer['id']][1] : null;
                                                                        $status = (isset($cell) and is_array($cell) and in_array($time, $cell) ) ? 'checked' : '';
                                                                        ?>
                                                                        <input {{$status}} name="schedule[{{$employer['id']}}][1][]" id="{{$employer['id']}}checkbox-0{{$i}}" value="{{ $time }}" type="checkbox" />
                                                                    </label>
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                        <tr>
                                                            <td>Вт.</td>
                                                            <td><input type="checkbox" class="check_row checkbox"></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <label class="label_check" for="{{$employer['id']}}checkbox-1{{$i}}">
                                                                        <?php
                                                                        $start_time = ($i<10) ? '0'.$i : $i;
                                                                        $end_time  = (($i+1)<10) ? '0'.($i+1) : ($i+1);
                                                                        $time = $start_time.':00-'.$end_time.':00';
                                                                        $cell = isset($schedule[$employer['id']][2]) ? $schedule[$employer['id']][2] : null;
                                                                        $status = (isset($cell) and is_array($cell) and in_array($time, $cell) ) ? 'checked' : '';
                                                                        ?>
                                                                        <input {{$status}} name="schedule[{{$employer['id']}}][2][]" id="{{$employer['id']}}checkbox-0{{$i}}" value="{{$time}}" type="checkbox" />
                                                                    </label>
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                        <tr>
                                                            <td>Ср.</td>
                                                            <td><input type="checkbox" class="check_row checkbox"></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <label class="label_check" for="{{$employer['id']}}checkbox-2{{$i}}">
                                                                        <?php
                                                                        $start_time = ($i<10) ? '0'.$i : $i;
                                                                        $end_time  = (($i+1)<10) ? '0'.($i+1) : ($i+1);
                                                                        $time = $start_time.':00-'.$end_time.':00';
                                                                        $cell = isset($schedule[$employer['id']][3]) ? $schedule[$employer['id']][3] : null;
                                                                        $status = (isset($cell) and is_array($cell) and in_array($time, $cell) ) ? 'checked' : '';
                                                                        ?>
                                                                        <input {{$status}} name="schedule[{{$employer['id']}}][3][]" id="{{$employer['id']}}checkbox-0{{$i}}" value="{{$time}}" type="checkbox" />
                                                                    </label>
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                        <tr>
                                                            <td>Чт.</td>
                                                            <td><input type="checkbox" class="check_row checkbox"></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <label class="label_check" for="{{$employer['id']}}checkbox-3{{$i}}">
                                                                        <?php
                                                                        $start_time = ($i<10) ? '0'.$i : $i;
                                                                        $end_time  = (($i+1)<10) ? '0'.($i+1) : ($i+1);
                                                                        $time = $start_time.':00-'.$end_time.':00';
                                                                        $cell = isset($schedule[$employer['id']][4]) ? $schedule[$employer['id']][4] : null;
                                                                        $status = (isset($cell) and is_array($cell) and in_array($time, $cell) ) ? 'checked' : '';
                                                                        ?>
                                                                        <input {{$status}} name="schedule[{{$employer['id']}}][4][]" id="{{$employer['id']}}checkbox-0{{$i}}" value="{{$time}}" type="checkbox" />
                                                                    </label>
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                        <tr>
                                                            <td>Пт.</td>
                                                            <td><input type="checkbox" class="check_row checkbox"></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <label class="label_check" for="{{$employer['id']}}checkbox-4{{$i}}">
                                                                        <?php
                                                                        $start_time = ($i<10) ? '0'.$i : $i;
                                                                        $end_time  = (($i+1)<10) ? '0'.($i+1) : ($i+1);
                                                                        $time = $start_time.':00-'.$end_time.':00';
                                                                        $cell = isset($schedule[$employer['id']][5]) ? $schedule[$employer['id']][5] : null;
                                                                        $status = (isset($cell) and is_array($cell) and in_array($time, $cell) ) ? 'checked' : '';
                                                                        ?>
                                                                        <input {{$status}} name="schedule[{{$employer['id']}}][5][]" id="{{$employer['id']}}checkbox-0{{$i}}" value="{{$time}}" type="checkbox" />
                                                                    </label>
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                        <tr>
                                                            <td>Сб.</td>
                                                            <td><input type="checkbox" class="check_row checkbox"></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <label class="label_check" for="{{$employer['id']}}checkbox-5{{$i}}">
                                                                        <?php
                                                                        $start_time = ($i<10) ? '0'.$i : $i;
                                                                        $end_time  = (($i+1)<10) ? '0'.($i+1) : ($i+1);
                                                                        $time = $start_time.':00-'.$end_time.':00';
                                                                        $cell = isset($schedule[$employer['id']][6]) ? $schedule[$employer['id']][6] : null;
                                                                        $status = (isset($cell) and is_array($cell) and in_array($time, $cell) ) ? 'checked' : '';
                                                                        ?>
                                                                        <input {{$status}} name="schedule[{{$employer['id']}}][6][]" id="{{$employer['id']}}checkbox-0{{$i}}" value="{{$time}}" type="checkbox" />
                                                                    </label>
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                        <tr>
                                                            <td>Вс.</td>
                                                            <td><input type="checkbox" class="check_row checkbox"></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <label class="label_check" for="{{$employer['id']}}checkbox-6{{$i}}">
                                                                        <?php
                                                                        $start_time = ($i<10) ? '0'.$i : $i;
                                                                        $end_time  = (($i+1)<10) ? '0'.($i+1) : ($i+1);
                                                                        $time = $start_time.':00-'.$end_time.':00';
                                                                        $cell = isset($schedule[$employer['id']][7]) ? $schedule[$employer['id']][7] : null;
                                                                        $status = (isset($cell) and is_array($cell) and in_array($time, $cell) ) ? 'checked' : '';
                                                                        ?>
                                                                        <input {{$status}} name="schedule[{{$employer['id']}}][7][]" id="{{$employer['id']}}checkbox-0{{$i}}" value="{{$time}}" type="checkbox" />
                                                                    </label>
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            @for($i = 0; $i <= 23; $i++)
                                                                <td>
                                                                    <input type="checkbox" class="check_col checkbox">
                                                                    {{  ($i<10) ? '0'.$i : $i  }}
                                                                    <br>
                                                                    {{  (($i+1)<10) ? '0'.($i+1) : ($i+1)  }}
                                                                </td>
                                                            @endfor
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Контакт
                            </header>
                            <div class="panel-body">
                                <div class="field_group col-md-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p>Имя контакта:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="contact_name">
                                                <option value="" <?php echo ($configuration['contact_name']=='') ? 'selected' : '' ;  ?> >Выберите поле</option>
                                                @foreach($site_fields as $key => $site_field)
                                                    @if($configuration['contact_name'] == $key)
                                                        <option value="{{$key}}" selected="">{{$site_field}}</option>
                                                    @else
                                                        <option value="{{$key}}">{{$site_field}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row top_buffer">
                                        <div class="col-md-3">
                                            <p>Авто имя:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" name="contact_auto_name" value="{{$configuration['contact_auto_name']}}">
                                        </div>
                                    </div>
                                    <div class="row top_buffer">
                                        <div class="col-md-5">
                                            <p>Поле формы</p>
                                        </div>
                                        <div class="col-md-5">
                                            <p>Поле CRM</p>
                                        </div>
                                    </div>
                                    <?php
                                    if(!isset($configuration['contact_fields'])){
                                        $contact_fields = false;
                                    }else{
                                        $contact_fields = unserialize($configuration['contact_fields']);
                                    }
                                    ?>
                                    @if(!$contact_fields)
                                        <div class="row field_param top_buffer">
                                            <div class="col-md-5">
                                                <select class="form-control" name="contact_field_site[]">
                                                    <option value="" selected="">Выберите поле</option>
                                                    @foreach($site_fields as $key2 => $site_field)
                                                        <option value="{{$key2}}">{{$site_field}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <select class="form-control" name="contact_field_amo[]">
                                                    <option value="" selected="">Выберите поле</option>
                                                    @foreach($custom_fields as $key3 => $value2)
                                                        @if($key3 == 'contacts')
                                                            @foreach($value2 as $custom_field)
                                                                <option value="{{$custom_field['id']}}">{{$custom_field['name']}}</option>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn btn-danger del_field_param"><span class="fa fa-trash-o"></span></button>
                                            </div>
                                        </div>
                                    @else
                                        @foreach($contact_fields as $key => $value)
                                            <div class="row field_param top_buffer">
                                                <div class="col-md-5">
                                                    <select class="form-control" name="contact_field_site[]">
                                                        <option value="">Выберите поле</option>
                                                        @foreach($site_fields as $key2 => $site_field)
                                                            <option value="{{$key2}}" <?php echo ($key2 == $value) ? 'selected' : '' ; ?> >{{$site_field}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="form-control" name="contact_field_amo[]">
                                                        <option value="">Выберите поле</option>
                                                        @foreach($custom_fields as $key3 => $value2)
                                                            @if($key3 == 'contacts')
                                                                @foreach($value2 as $custom_field)
                                                                    <option value="{{$custom_field['id']}}" <?php echo ($key == $custom_field['id']) ? 'selected' : '' ; ?> >{{$custom_field['name']}}</option>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-danger del_field_param"><span class="fa fa-trash-o"></span></button>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                    <div class="row top_buffer bottom_buffer">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-default add_field_param"><span class="fa fa-plus"></span>Добавить параметр</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="panel">
                            <header class="panel-heading">
                                Сделка
                            </header>
                            <div class="panel-body">
                                <div class="field_group col-md-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p>Название сделки:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="lead_name">
                                                <option value="" <?php echo ($configuration['lead_name']=='') ? 'selected' : '' ;  ?> >Выберите поле</option>
                                                @foreach($site_fields as $key => $site_field)
                                                    <option value="{{$key}}" <?php echo ($key == $configuration['lead_name']) ? 'selected' : '' ; ?> >{{$site_field}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row top_buffer">
                                        <div class="col-md-3">
                                            <p>Авто название:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" name="lead_auto_name" value="{{$configuration['lead_auto_name']}}">
                                        </div>
                                    </div>
                                    <div class="row top_buffer">
                                        <div class="col-md-3">
                                            <p>Статус сделки:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="lead_status">
                                                @foreach($leads_statuses as $pipeline)
                                                    <option disabled="">{{$pipeline['name']}}</option>
                                                    @foreach($pipeline['statuses'] as $lead_status)
                                                        <option value="{{$lead_status['id']}}" <?php echo ($lead_status['id'] == $configuration['lead_status']) ? 'selected' : '' ; ?> >{{$lead_status['name']}}</option>
                                                    @endforeach
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                    if(!isset($configuration['lead_fields'])){
                                        $lead_fields = false;
                                    }else{
                                        $lead_fields = unserialize($configuration['lead_fields']);
                                    }
                                    ?>
                                    @if(!$lead_fields)
                                        <div class="lead_fields" style="display: none;">
                                            <div class="row top_buffer">
                                                <div class="col-md-5">
                                                    <p>Поле формы</p>
                                                </div>
                                                <div class="col-md-5">
                                                    <p>Поле CRM</p>
                                                </div>
                                            </div>
                                            <div class="row field_param top_buffer">
                                                <div class="col-md-5">
                                                    <select class="form-control" name="lead_fields_site[]">
                                                        <option value="" selected="">Выберите поле</option>
                                                        @foreach($site_fields as $key => $site_field)
                                                            <option value="{{$key}}">{{$site_field}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="form-control" name="lead_fields_amo[]">
                                                        <option value="" selected="">Выберите поле</option>
                                                        @foreach($custom_fields as $key => $value)
                                                            @if($key == 'leads')
                                                                @foreach($value as $custom_field)
                                                                    <option value="{{$custom_field['id']}}">{{$custom_field['name']}}</option>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-danger del_field_param"><span class="fa fa-trash-o"></span></button>
                                                </div>
                                            </div>
                                            <div class="row top_buffer bottom_buffer">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-default add_field_param"><span class="fa fa-plus"></span>Добавить параметр</button>
                                                </div>
                                            </div>
                                            <div class="row top_buffer bottom_buffer">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-default hide_lead_fields"><span class="fa fa-plus"></span>Скрыть поля</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row top_buffer bottom_buffer">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-default show_lead_fields"><span class="fa fa-plus"></span>Показать поля</button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="lead_fields">
                                            <div class="row top_buffer">
                                                <div class="col-md-3">
                                                    <p>Поле формы</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>Поле CRM</p>
                                                </div>
                                            </div>
                                            @foreach($lead_fields as $i => $field)
                                                <div class="row field_param top_buffer">
                                                    <div class="col-md-3">
                                                        <select class="form-control lead_fields_site" name="lead_fields_site[]">
                                                            <option value="">Выберите поле</option>
                                                            @foreach($site_fields as $key => $site_field)
                                                                <option value="{{$key}}" <?php echo ($key == $field) ? 'selected' : ''; ?> >{{$site_field}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control lead_fields_amo" name="lead_fields_amo[]">
                                                            <option value="">Выберите поле</option>
                                                            @foreach($custom_fields as $key => $value)
                                                                @if($key == 'leads')
                                                                    @foreach($value as $custom_field)
                                                                        <option value="{{$custom_field['id']}}" <?php echo ($custom_field['id'] == $i) ? 'selected' : ''; ?> >{{$custom_field['name']}}</option>
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button type="button" class="btn btn-danger del_field_param"><span class="fa fa-trash-o"></span></button>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div class="row top_buffer bottom_buffer">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-default add_field_param"><span class="fa fa-plus"></span>Добавить параметр</button>
                                                </div>
                                            </div>
                                            <div class="row top_buffer bottom_buffer">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-default hide_lead_fields"><span class="fa fa-plus"></span>Скрыть поля</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row top_buffer bottom_buffer">
                                            <div class="col-md-12">
                                                <button type="button" style="display: none;" class="btn btn-default show_lead_fields"><span class="fa fa-plus"></span>Показать поля</button>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </section>
                        <section class="panel">
                            <header class="panel-heading">
                                Задача
                            </header>
                            <div class="panel-body">
                                <div class="field_group col-md-12 bottom_buffer">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p>Тип задачи:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="task_type">
                                                @foreach($task_types as $task_type)
                                                    <option value="{{$task_type['id']}}" <?php echo ($task_type['id'] == $configuration['task_type']) ? 'selected' : '' ; ?> >{{$task_type['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row top_buffer">
                                        <div class="col-md-3">
                                            <p>Комметарий:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" name="task_comment" value="{{$configuration['task_comment']}}">
                                        </div>
                                    </div>
                                    <div class="row top_buffer">
                                        <div class="col-md-3">
                                            <p>Выполнить до:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="task_until">
                                                <option value="1" <?php echo (1 == $configuration['task_until']) ? 'selected' : '' ; ?>>Сегодня</option>
                                                <option value="2" <?php echo (2 == $configuration['task_until']) ? 'selected' : '' ; ?>>Завтра</option>
                                                <option value="3" <?php echo (3 == $configuration['task_until']) ? 'selected' : '' ; ?>>Через неделю</option>
                                                <option value="4" <?php echo (4 == $configuration['task_until']) ? 'selected' : '' ; ?>>Через месяц</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="panel">
                            <header class="panel-heading">
                                Дополнительно
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <p>Контроль дублей (контактов):</p>
                                    </div>
                                    <div class="col-md-8">
                                        <select class="form-control" name="dublicate_controll">
                                            <option value="1" <?php echo (1 == $configuration['dublicate_controll']) ? 'selected' : '' ; ?>>Создаем задачу в контакте</option>
                                            <option value="2" <?php echo (2 == $configuration['dublicate_controll']) ? 'selected' : '' ; ?>>Создаем задачу в существующей сделке(в первой открытой)</option>
                                            <option value="3" <?php echo (3 == $configuration['dublicate_controll']) ? 'selected' : '' ; ?>>Создаем новую сделку с задачей</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="panel">
                            <header class="panel-heading">
                                @if(empty($validations))
                                    Скопируйте код для Вашего сайта
                                @else
                                    Исправте замечания
                                @endif
                            </header>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(empty($validations))
                                                <h3>Код для html отправки форм</h3>
                                                <pre>
&lt;script type="text/javascript" src="{{env('APP_URL')}}/public/integration.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript"&gt;INTEGRATION.init([{{$site['id']}}]);INTEGRATION.integrate();&lt;/script&gt;
                                                </pre>
                                                <h3>Код для ajax отправки форм</h3>
                                                <pre>
var terms = {};
var QueryString = function () {
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();
terms = QueryString;
var p_form = < FORM OBJECT >; //insert you form object which will send here
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
if (this.readyState == 4 && this.status == 200) {
console.log(this.responseText);
}
};
var kvpairs = [];
for ( var i = 0; i < p_form.elements.length; i++ ) {
var e = p_form.elements[i];
kvpairs.push(encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value));
}
var queryString = kvpairs.join("&");
var queryTerms = '';
if(typeof(terms.utm_source) != "undefined" && terms.utm_source != '' ){
    queryTerms += '&utm_source='+terms.utm_source;
}
if(typeof(terms.utm_medium) != "undefined" && terms.utm_medium != '' ){
    queryTerms += '&utm_medium='+terms.utm_medium;
}
if(typeof(terms.utm_term) != "undefined" && terms.utm_term != '' ){
    queryTerms += '&utm_term='+terms.utm_term;
}
if(typeof(terms.utm_content) != "undefined" && terms.utm_content != '' ){
    queryTerms += '&utm_content='+terms.utm_content;
}
if(typeof(terms.utm_campaign) != "undefined" && terms.utm_campaign != '' ){
    queryTerms += '&utm_campaign='+terms.utm_campaign;
}
xhttp.open("GET", '{{env('APP_URL')}}/amo/integration/{{$site['id']}}?'+queryString+queryTerms, true);
xhttp.send();
                                                </pre>
                                            @else
                                                @foreach($validations as $validation)
                                                    <p class="text-danger">{{$validation}}</p>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
        </section>
    </section>
    <!--main content end-->
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Изменить название</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" action="{{ url('/site_integration/change_name/'.$site['id']) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="url" class="col-md-3 control-label">Новое имя</label>
                            <div class="col-md-9">
                                <input id="url" type="text" required class="form-control" name="new_name" value="{{$site['short_name']}}">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Изменить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>
@endsection