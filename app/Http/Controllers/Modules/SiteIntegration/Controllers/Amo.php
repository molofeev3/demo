<?php

namespace App\Http\Controllers\Modules\SiteIntegration\Controllers; // define namespace

use App\Http\Controllers\Modules\SiteIntegration\SiteIntegration;
use App\User;

require_once app_path().'/Amo/map.php';


class Amo
{
    public $domain = 'test';
    public $api_key = 'test';
    public $email = 'test';



    public function __construct($domain, $api_key, $email){
        $this->domain = $domain;
        $this->api_key = $api_key;
        $this->email = $email;
    }

    public function auth(){
        $user=array(
            'USER_LOGIN'=>$this->email,
            'USER_HASH'=>$this->api_key
        );

        $subdomain=$this->domain;

        $link='https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';
        $this->amo_curl($user, $link);
    }

    public function get_account_data()
    {
        $this->auth();
        $subdomain=$this->domain;
        $link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/accounts/current';
        $data = '';
        return $this->amo_curl($data, $link);
    }

    public function get_select_fields()
    {
        $data = $this->get_account_data();
        $fields = array();
        foreach ($data['account']['custom_fields']['leads'] as $field) {
            if($field['type_id']==4){
                $fields[$field['id']] = $field['name'];
            }
        }
        return $fields;
    }

    public static function carousel_distribution($configuration, $responsible_user){
        $responsible_user_id = 0;
        $distributions = $configuration->distributions;
        $queue = array();
        if(isset($distributions) and !empty($distributions)){
            $queue = unserialize($distributions);
        }else{
            foreach ($responsible_user as  $item) {
                $queue[$item] = 1;
            }
        }
        $flag  = true;
        foreach ($queue as $user => $state) {
            if ($state == 1) {
                $flag = false;
            }
        }
        if ($flag===true or count($queue) != count($responsible_user)) {
            foreach ($responsible_user as  $item) {
                $queue[$item] = 1;
            }
        }
        foreach ($queue as $user => $state){
            if($state == 1){
                $responsible_user_id = $user;
                $queue[$user] = 0;
                break;
            }
        }
        $configuration->distributions = serialize($queue);
        $configuration->save();
        return $responsible_user_id;
    }

    public static function open_leads_distribution(\AmoCRM $amo_crm, $responsible_user, $amo_data){
        $leads_statuses = array();
        foreach ($amo_data['account']['leads_statuses'] as $leads_status){
            if(!in_array($leads_status['id'], array(142, 143))){
                $leads_statuses[] = $leads_status['id'];
            }
        }
        $statuses = '&status[]='.implode('&status[]=', $leads_statuses);
        $user_leads = array();
        foreach ($responsible_user as $user){
            $leads = $amo_crm->getLeadByQuery('responsible_user_id='.$user.'&'.$statuses);
            $user_leads[$user] = count($leads);
        }
        $responsible_user_id = array_keys($user_leads, min($user_leads))[0];
        return $responsible_user_id;
    }


    public static function zero_leads_distribution(\AmoCRM $amo_crm, $responsible_user, $configuration, $amo_data, $user){
        $responsible_user_id = 0;
        foreach ($responsible_user as $employer){
            $leads = $amo_crm->getLeadByQuery('responsible_user_id='.$employer.'&status='.$configuration->lead_status);
            if (count($leads) == 0){
                $responsible_user_id = $employer;
                break;
            }
        }
        if($responsible_user_id == 0){
            foreach ($amo_data['account']['users'] as $employer){
                if($employer['login'] == $user->amo_email){
                    $responsible_user_id = $employer['id'];
                }
            }
        }
        return $responsible_user_id;
    }

    public static function skill_distribution($configuration, $responsible_user){
        $responsible_user_id = 0;
        $distributions = $configuration->distributions;
        if (isset($distributions) and $distributions != ''){
            $user_skill = unserialize($distributions);
        }else{
            $user_skill = array();
            foreach (unserialize($configuration->schedule) as $key => $employer){
                if(in_array($key, $responsible_user)) {
                    $user_skill[$key]['skill'] = $employer['skill'];
                    $user_skill[$key]['leads'] = 0;
                }
            }
        }
        $flag = false;

        $keys = array_keys($user_skill);
        foreach ($keys as $key => $user){
            if(isset($keys[$key+1])){
                if( ($user_skill[$user]['leads'] < $user_skill[$user]['skill']) and ($user_skill[$keys[$key+1]]['leads'] >= $user_skill[$user]['leads']) ){
                    $responsible_user_id = $user;
                    $user_skill[$user]['leads'] += 1;
                    $flag = true;
                    break;
                }
            }else{
                if( ($user_skill[$user]['leads'] < $user_skill[$user]['skill']) ){
                    $responsible_user_id = $user;
                    $user_skill[$user]['leads'] += 1;
                    $flag = true;
                    break;
                }
            }
        }
        if(!$flag){
            foreach ($user_skill as $user => $data){
                $user_skill[$user]['leads'] = 0;
                if($user == $responsible_user[0]){
                    $user_skill[$user]['leads'] += 1;
                    $responsible_user_id = $user;
                }
            }
        }
        $configuration->distributions = serialize($user_skill);
        $configuration->save();
        return $responsible_user_id;

    }


    public static function integrate($site_id, \Illuminate\Http\Request $request){
        $site = SiteIntegration::find($site_id);
        if(!isset($site)){
            die('undefined site');
        }
        if(strpos($request->server('HTTP_REFERER'), $site->url) === false){
            die('bad request site url');
        }

        $log = $site->log;
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ' undefined ';
        $http_referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ' undefined ';
        $remote_addr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ' undefined ';
        $site->log = $log.'<p>['.date('d-m-Y H:i:s', time()).'] '.$user_agent.' | '.$http_referer.' | '.serialize($request->all()).' | '.$remote_addr.'</p>';
        $site->save();

        $configuration = $site;
        $user = User::find($site->user_id);
        $form_args = $request->all();
        $amo_crm = \AmoCRM::get_instance($user['amo_email'], $user['amo_hash'], $user['amo_domain']);
        $contact_fields = unserialize($configuration->contact_fields);
        $flag = false;
        if(!empty($contact_fields)){
            foreach ($contact_fields as $key => $contact_field){
                if(isset($form_args[$contact_field]) and $form_args[$contact_field] != ''){
                    $contact_by_email = false;
                    $contact_by_phone = false;
                    foreach ($amo_crm->getAccountInfo()["account"]["custom_fields"]["contacts"] as $account_field) {
                        if ($account_field['id'] == $key) {
                            if($account_field['code'] == 'PHONE'){
                                $contact_by_phone = $amo_crm->getContactByPhones($form_args[$contact_field]);
                            }elseif ($account_field['code'] == 'EMAIL'){
                                $contact_by_email = $amo_crm->getContactByEmails($form_args[$contact_field]);
                            }
                        }
                    }
                    if(($contact_by_email != false) or ($contact_by_phone != false)){
                        $flag = true;
                        break;
                    }
                }
            }
        }

        if($flag){

            if($contact_by_email == false and $contact_by_phone != false){
                $contact_by_email = $contact_by_phone;
            }
            /*
        * Updating contact
        * */

            if(count($contact_fields) != 1){
                $fields_data = array();
                foreach ($contact_fields as $id => $contact_field){
                    foreach ($form_args as $name => $value){
                        if($contact_field == $name){
                            foreach ($amo_crm->getAccountInfo()["account"]["custom_fields"]["contacts"] as $account_field){
                                if($account_field['id'] == $id){
                                    $fields_data[$id] = array(
                                        'form_name' => $name,
                                        'form_value' => $value,
                                        'platform_name' => $contact_field,
                                        'platform_id' => $id,
                                        'account_id' => $account_field['id'],
                                        'account_code' => $account_field['code'],
                                        'account_name' => $account_field['name'],
                                        'amo_id' => '',
                                        'amo_code' => '',
                                        'amo_name' => '',
                                        'amo_values' => ''
                                    );
                                }
                            }
                            foreach ($contact_by_email['custom_fields'] as $custom_field){
                                if( $custom_field['id'] == $id){
                                    $fields_data[$id]['amo_id'] = $custom_field['id'];
                                    $fields_data[$id]['amo_code'] = $custom_field['code'];
                                    $fields_data[$id]['amo_name'] = $custom_field['name'];
                                    $fields_data[$id]['amo_values'] = $custom_field['values'];

                                }
                            }
                        }
                    }
                }


                $update_fields = array();

                foreach ($fields_data as $id => $item){
                    if($item['amo_code'] == 'PHONE'){
                        $update_fields[$id][] = \Helper::clearPhone($item['form_value']);
                        if(!empty($item['amo_values'])){
                            foreach ($item['amo_values'] as $amo_value){
                                if(substr(\Helper::clearPhone($item['form_value']), -7) != substr(\Helper::clearPhone($amo_value['value']), -7)){
                                    $update_fields[$id][] = \Helper::clearPhone($amo_value['value']);
                                }
                            }
                        }
                    }elseif($item['amo_code'] == 'EMAIL'){
                        $update_fields[$id][] = mb_strtolower($item['form_value']);
                        if(!empty($item['amo_values'])){
                            foreach ($item['amo_values'] as $amo_value){
                                if(mb_strtolower($item['form_value']) != mb_strtolower($amo_value['value'])){
                                    $update_fields[$id][] = $amo_value['value'];
                                }
                            }
                        }
                    }else{
                        $update_fields[$id][] = $item['form_value'];
                        if(!empty($item['amo_values'])){
                            foreach ($item['amo_values'] as $amo_value){
                                if(mb_strtolower($item['form_value']) != $amo_value['value']){
                                    $update_fields[$id][] = $amo_value['value'];
                                }
                            }
                        }
                    }
                }

                if(!empty($update_fields)){
                    $update_contact = $amo_crm->createContactInstance();
                    $update_contact->setId($contact_by_email['id']);
                    $update_contact->setLastModified(time());
                    $uccf = new \CustomFields();
                    foreach ($update_fields as $id => $values){
                        if(count($values) == 1){
                            $values = $values[0];
                        }
                        $cf = new \CustomField(8, $values, $id);
                        $uccf->push($cf);
                    }
                    $update_contact->setCustomFields($uccf);
                    $update_contact->update();
                }

            }

            /*
             * End of updating contact
             */

            $responsible_user = $amo_crm->getContactById($contact_by_email['id'])[0]['responsible_user_id'];

            //new logic
            if(isset($configuration->tasks) and $configuration->tasks == 'on'){
                switch ($configuration->dublicate_controll){
                    case 1:

                        Amo::create_task_in_contact($amo_crm, $site_id, $contact_by_email['id'], $responsible_user);
                        break;
                    case 3:

                        Amo::create_lead_and_task($amo_crm, $site_id, $request, $contact_by_email['id'], $responsible_user, $contact_by_email['linked_leads_id']);
                        break;
                }
            }else{
                $lead_res = Amo::create_lead($amo_crm, $configuration, $form_args, $user);
                $existing_leads = $contact_by_email['linked_leads_id'];
                $existing_leads[] = $lead_res['id'];
                $existing_contact = $amo_crm->createContactInstance();
                $existing_contact->setId($contact_by_email['id']);
                $existing_contact->setLastModified(time());
                $existing_contact->setLinkedLeadsId($existing_leads);
                $existing_contact->update();
            }
            //end of new logic

        }else{

            if(isset($configuration->tasks) and $configuration->tasks == 'on'){
                Amo::simple_integration($amo_crm, $site_id, $request);
            }else{
                $lead_res = Amo::create_lead($amo_crm, $configuration, $form_args, $user);
                Amo::create_contact($amo_crm, $configuration, $form_args, $lead_res['id'], $lead_res['responsible_user']);
            }
        }
    }


    public static function get_responsible_user(\AmoCRM $amo_crm, $configuration, $amo_data, $user){
        $responsible_user = array();
        $responsible_user_id = 0;
        $current_time = date('H:i', time());
        $day_number = date('N', time());

        $schedule = unserialize($configuration->schedule);
        foreach ($schedule as $key => $employer){
            if(isset($employer['active'])){
                for($i = 1; $i <= 7; $i++){
                    if(isset($employer[$i]) and $i == $day_number){
                        foreach ($employer[$i] as $time){
                            if (strtotime(explode('-', $time)[0]) <= strtotime($current_time) and strtotime(explode('-', $time)[1]) >= strtotime($current_time)) {
                                $responsible_user[] = $key;
                            }
                        }
                    }
                }
            }
        }
        if (empty($responsible_user)){
            foreach ($amo_data['account']['users'] as $employer){
                if($employer['login'] == $user->amo_email){
                    $responsible_user_id = $employer['id'];
                }
            }
        }else{
            if (count($responsible_user)==1) {
                $responsible_user_id = $responsible_user[0];
            }else{
                switch ($configuration->distribution_rule){
                    case '1':
                        $responsible_user_id = Amo::carousel_distribution($configuration, $responsible_user);
                        break;
                    case '2':
                        $responsible_user_id = Amo::skill_distribution($configuration, $responsible_user);
                        break;
                    case '3':
                        $responsible_user_id = Amo::zero_leads_distribution($amo_crm, $responsible_user, $configuration, $amo_data, $user);
                        break;
                    case '4':
                        $responsible_user_id = Amo::open_leads_distribution($amo_crm, $responsible_user, $amo_data);
                        break;
                }
            }
        }
        return $responsible_user_id;
    }

    public static function create_lead(\AmoCRM $amo_crm, $configuration, $form_args, $user, $responsible_user=null){
        $amo_data = $amo_crm->getAccountInfo();
        $lead_inst = $amo_crm->createLeadInstance();
        $lead_name = (isset($configuration->lead_name) and ($configuration->lead_name !='') and isset($form_args[$configuration->lead_name]) and  ($form_args[$configuration->lead_name]!='')) ? $form_args[$configuration->lead_name] : $configuration->lead_auto_name;
        $lead_inst->setName($lead_name);
        if(isset($configuration->lead_price) and ($configuration->lead_price != '') and (isset($form_args[$configuration->lead_price])) and ($form_args[$configuration->lead_price] != '')){
            $lead_inst->setPrice($form_args[$configuration->lead_price]);
        }

        if($responsible_user){
            $lead_inst->setResponsibleUserId($responsible_user);
        }else{
            $responsible_user = Amo::get_responsible_user($amo_crm, $configuration, $amo_data, $user);
            $lead_inst->setResponsibleUserId($responsible_user);
        }
        $lead_inst->setStatus($configuration->lead_status);
        //start of custom fields
        $customFields = new \CustomFields();


        if(isset($configuration->utm_terms) and !empty($configuration->utm_terms)){ // set utm terms
            $utm_terms = unserialize($configuration->utm_terms);
            if(isset($utm_terms) and is_array($utm_terms)){
                foreach ($utm_terms as $term => $term_field_id){
                    if(empty($term_field_id)){
                        continue;
                    }else{
                        if(isset($form_args[$term]) and $form_args[$term] != ''){
                            $termField = new \CustomField(1, $form_args[$term], $term_field_id);
                            $customFields->push($termField);
                        }
                    }
                }
            }
        }

        $lead_fields = unserialize($configuration['lead_fields']);
        if (!empty($lead_fields)){
            foreach ($lead_fields as $key => $lead_field){
                foreach ($amo_data['account']['custom_fields']['leads'] as $field){
                    if($field['id'] == $key){
                        if(isset($form_args[$lead_field]) and $form_args[$lead_field] != ''){
                            $customField = new \CustomField($field['type_id'], $form_args[$lead_field], $key);
                            $customFields->push($customField);
                        }
                    }
                }
            }
        }
        $lead_inst->setCustomFields($customFields);
        //end of custom fields

        $addIdd = $lead_inst->add();

        return array(
            'id' => $addIdd,
            'responsible_user' => $responsible_user
        );
    }

    public static function create_contact(\AmoCRM $amo_crm, $configuration, $form_args, $addIdd, $responsible_user){
        $amo_data = $amo_crm->getAccountInfo();
        $contact_inst = $amo_crm->createContactInstance();

        $contact_name = (isset($configuration->contact_name) and isset($form_args[$configuration->contact_name]) and ($form_args[$configuration->contact_name]) !=='') ? $form_args[$configuration->contact_name] : $configuration->contact_auto_name;

        $contact_inst->setName($contact_name);
        $contact_inst->setLinkedLeadsId(array($addIdd));
        $contact_inst->setResponsibleUserId($responsible_user);


        $contact_fields = unserialize($configuration['contact_fields']);
        if (!empty($contact_fields)){
            $contactCF = new \CustomFields();
            foreach ($contact_fields as $key => $contact_field){
                foreach ($amo_data['account']['custom_fields']['contacts'] as $field){
                    if($field['id'] == $key){
                        if(isset($form_args[$contact_field]) and $form_args[$contact_field] != ''){
                            if($field['code'] == 'PHONE'){
                                $cf = new \CustomField($field['type_id'], \Helper::clearPhone($form_args[$contact_field]), $key);
                            }else{
                                $cf = new \CustomField($field['type_id'], $form_args[$contact_field], $key);
                            }

                            $contactCF->push($cf);
                        }
                    }
                }
            }
            $contact_inst->setCustomFields($contactCF);
        }
        return $contact_inst->add();
    }

    public static function create_task(\AmoCRM $amo_crm, $configuration, $element_id, $element_type, $user, $responsible_user=null){
        $task_inst = $amo_crm->createTaskInstance();
        $task = $task_inst;
        $task->setTaskType($configuration->task_type);
        $task->setText($configuration->task_comment);
        if($responsible_user){
            $task->setResponsibleUserId($responsible_user);
        }else{
            $task->setResponsibleUserId(Amo::get_responsible_user($amo_crm, $configuration, $amo_crm->getAccountInfo(), $user));
        }
        $task_until = $configuration->task_until;
        switch ($task_until){
            case 1:
                $task->setCompleteTill(strtotime(date('23:59:59 d-m-Y',time())));
                break;
            case 2:
                $task->setCompleteTill(strtotime(date( '23:59:59 '.date('d-m-Y',strtotime('+1 day')), time() )));
                break;
            case 3:
                $task->setCompleteTill(strtotime(date( '23:59:59 '.date('d-m-Y',strtotime('+1 week')), time() )));
                break;
            case 4:
                $task->setCompleteTill(strtotime(date( '23:59:59 '.date('d-m-Y',strtotime('+1 month')), time() )));
                break;
        }
        $task->setElementType($element_type);
        $task->setElementId($element_id);
        $task->add();
    }


    public static function simple_integration(\AmoCRM $amo_crm, $id, $request){
        $configuration = SiteIntegration::find($id);
        $user = $configuration->user;
        $form_args = $request->all();
        $addIdd = Amo::create_lead($amo_crm, $configuration, $form_args, $user);
        $new_contact_id = Amo::create_contact($amo_crm, $configuration, $form_args, $addIdd['id'], $addIdd['responsible_user']);
        Amo::create_task($amo_crm, $configuration, $new_contact_id, 1, $user, $addIdd['responsible_user']);
    }


    public static function create_task_in_contact($amo_crm, $id, $contact_id, $responsible_user){
        $configuration = SiteIntegration::find($id);
        $user = $configuration->user;
        Amo::create_task($amo_crm, $configuration, $contact_id, 1, $user, $responsible_user);
    }

    public static function create_task_in_lead($amo_crm, $id, $lead_id, $responsible_user){
        $configuration = SiteIntegration::find($id);
        $user = $configuration->user;
        Amo::create_task($amo_crm, $configuration, $lead_id, 2, $user, $responsible_user);
    }

    public static function create_lead_and_task(\AmoCRM $amo_crm, $id, $request, $contact_id, $responsible_user, $linked_leads){
        $configuration = SiteIntegration::find($id);
        $user = $configuration->user;
        $form_args = $request->all();
        $addIdd  = Amo::create_lead($amo_crm, $configuration, $form_args, $user);
        $contact_inst = $amo_crm->createContactInstance();
        $contact = $contact_inst;
        $contact->setId($contact_id);
        $contact->setLastModified(time());
        $linked_leads[] = $addIdd['id'];
        $contact->setLinkedLeadsId($linked_leads);
        $contact->update();
        Amo::create_task($amo_crm, $configuration, $addIdd['id'], 2, $user);
    }




    public function set_webhook($id)
    {
        $this->auth();
        $data = array (
            'request' =>
                array (
                    'webhooks' =>
                        array (
                            'subscribe' =>
                                array (
                                    array (
                                        'url' => url('/').'/webhooks/webhook-'.$id.'.php',
                                        'events' =>
                                            array (
                                                'add_lead',
                                            ),
                                    ),
                                ),
                        ),
                ),
        );

        $subdomain = $this->domain;
        $link = 'https://'.$subdomain.'.amocrm.ru/private/api/v2/json/webhooks/subscribe/';
        return $this->amo_curl($data, $link);

    }


    public function amo_curl($data, $link)
    {
        $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        if ($data!='') {
            curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        }
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

        $out=curl_exec($curl);
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);

        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try
        {
            if($code!=200 && $code!=204)
                return false;
            //throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
        }
        catch(Exception $E)
        {
            return false;
            die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());

        }

        $Response=json_decode($out,true);
        $Response=$Response['response'];
        return $Response;
    }
}
