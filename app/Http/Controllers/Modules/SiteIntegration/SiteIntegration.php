<?php
/*
 *
 * Example of defining Model class
 *
 */
namespace App\Http\Controllers\Modules\SiteIntegration;

use Illuminate\Database\Eloquent\Model;

class SiteIntegration extends Model
{
    protected $table = 'site_integration';

    protected $fillable = [
        'contact_name', 'contact_auto_name', 'contact_fields', 'lead_name', 'lead_auto_name', 'lead_status', 'lead_fields', 'task_type', 'task_comment', 'task_until', 'dublicate_controll', 'distribution_rule',  'schedule', 'short_name', 'url', 'distributions', 'utm_terms', 'tasks', 'lead_price', 'user_id', 'log'
    ];

}