<?php
/**
 * Created by PhpStorm.
 * User: molofeev
 * Date: 17.05.17
 * Time: 15:56
 */
Route::group(['namespace' => 'Modules\SiteIntegration\Controllers'], function()
{
    Route::get('/site_integration', ['uses'=>'SiteIntegrationController@index']);
    Route::get('/site_integration/{id}', ['uses'=>'SiteIntegrationController@site'])->where(['id'=>'[0-9]+']);
    Route::get('/site_integration/add', ['uses'=>'SiteIntegrationController@add_site']);
    Route::post('/site_integration/add_new_site', ['uses'=>'SiteIntegrationController@add_new_site']);
    Route::get('/site_integration/del_site/{id}', ['uses'=>'SiteIntegrationController@del_site'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration/change_name/{id}' , ['uses'=>'SiteIntegrationController@change_name']);
    Route::post('/site_integration/save/{id}', ['uses'=>'SiteIntegrationController@save'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration/employers/disactive/{id}', ['uses'=>'SiteIntegrationController@getDisactiveEmployers'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration/employers/active/{id}', ['uses'=>'SiteIntegrationController@getActiveEmployers'])->where(['id'=>'[0-9]+']);
    Route::post('/site_integration/employers/all/{id}', ['uses'=>'SiteIntegrationController@getAllEmployers'])->where(['id'=>'[0-9]+']);

    Route::get('/site_integration/{id}/log', ['uses'=>'SiteIntegrationController@getLog'])->where(['id'=>'[0-9]+']);
});
Route::get('/amo/integration/{id}', array('middleware' => 'guest', function($id, \Illuminate\Http\Request $request) {
        return App\Http\Controllers\Modules\SiteIntegration\Controllers\Amo::integrate($id, $request);
//    return 'Hello';
}));