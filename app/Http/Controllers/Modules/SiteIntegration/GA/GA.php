<?php

namespace App\Http\Controllers\Modules\GA;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GA extends Model
{
    protected $table = 'ga_data';

    protected $fillable = [
        'id', 'ga', 'user_id'
    ];

}