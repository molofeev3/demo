<?php

namespace App\Http\Controllers\Modules\GA\Controllers; // define namespace


use App\Http\Controllers\Controller;
use App\Menu;
use App\Module;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema; // use base controller class


class MainController extends Controller // example of main controller for install and uninstall module
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function install()
    {
        Schema::create('ga_data', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('ga')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });

        $menu_item = new Menu(); // example of add module to menu
        $menu_item->title = 'Интеграция с GA';
        $menu_item->url = 'ga';
        $menu_item->parent = 3; //subitem for Modules item, subitems hasnt ico
        $menu_item->save();

        $module = new Module();// example of registration module
        $module->name = 'GA';
        $module->save();

        return redirect('/modules');
    }

    public function uninstall(){
        Schema::drop('ga_data');
        Menu::where('title', '=', 'Интеграция с GA')->delete(); //example of removing module from menu

        Module::where('name', '=', 'GA')->delete(); // example of understating module

        return redirect('/modules');
    }
}
