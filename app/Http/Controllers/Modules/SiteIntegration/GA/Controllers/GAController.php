<?php

namespace App\Http\Controllers\Modules\GA\Controllers;

use App\Http\Controllers\Modules\GA\GA;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
require_once app_path().'/Amo/map.php';

class GAController extends Controller
{
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->middleware('auth', ['except' => [
            'webhook'
        ]]);
        $this->middleware('amo', ['except' => [
            'webhook'
        ]]);
        $this->request = $request;
    }

    public function index($errors=null){
        $user = Auth::user();
        $ga = GA::where('user_id', '=', $user->id)->first();
        if(!isset($ga)){
            $ga = GA::create([
                'user_id' => $user->id,
                'ga' => serialize(array())
            ]);
        }
        $ga = $ga->toArray()['ga'];

        $amo = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain);
        $amo_account = $amo->getAccountInfo()['account'];
        $data = array(
            'amo_fields' => $amo_account['custom_fields'],
            'managers' => $amo_account['users'],
            'leads_statuses' => $amo_account['pipelines'],
            'user' => $user,
            'ga' => unserialize($ga),
            'errors' => $errors
        );
        return view('GA.Views.index', $data);
    }

    public function update(){
        $input = $this->request->all();
        $user = Auth::user();
        $ga = GA::where('user_id', '=', $user->id)->first();
        if(empty($ga)){
            $ga = new GA();
        }
        $ga->ga = serialize($input['ga']);
        $ga->user_id = $user->id;

        $ga->save();
        return $this->gaValidation($input['ga']);
    }

    public function gaValidation($data){
        $errors = [];
        if(empty($data['cd_index'])){
            $errors['cd_index'] = 'Это поле обьязательно к заполнению';
        }
        if(!empty($data['fail_status']) and empty($data['cm_index'])){
            $errors['cm_index'] = 'Это поле обязательно при использовании неуспешных статусов';
        }
        if(!empty($data['fail_status']) and empty($data['site_url'])){
            $errors['site_url'] = 'Это поле обязательно при использовании неуспешных статусов';
        }
        if(empty($data['clientid'])){
            $errors['clientid'] = 'Это поле обьязательно к заполнению';
        }
        if(empty($data['view_id'])){
            $errors['view_id'] = 'Это поле обьязательно к заполнению';
        }

        if(empty($errors)){
            $user = Auth::user();
            $amo = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain);

            $data = array (
                'request' =>
                    array (
                        'webhooks' =>
                            array (
                                'subscribe' =>
                                    array (
                                        array (
                                            'url' => url('/').'/ga/webhook',
                                            'events' =>
                                                array (
                                                    'status_lead',
                                                ),
                                        ),
                                    ),
                            ),
                    ),
            );

            $link = 'https://'.$user->amo_domain.'.amocrm.ru/private/api/v2/json/webhooks/subscribe/';
            $amo->amoCurl($link, $data);
        }

        return $this->index($errors);
    }


    public function webhook(){
        file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] webhook activated'.PHP_EOL, FILE_APPEND);
        $input = $this->request->all();
        $ga = unserialize(GA::first()->toArray()['ga']);
        if(isset($ga) and isset($ga['clientid']) and !empty($ga['clientid'])){
            file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] data exist in DB and clientID not empty'.PHP_EOL, FILE_APPEND);

            $price = null;
            $gaClientId = null;
            $ti = null;
            $gclid = '';

            if(isset($ga['success_status']) and !empty($ga['success_status']) and is_array($ga['success_status'])){
                $success_pipelines = array();
                $success_statuses = array();
                foreach ($ga['success_status'] as $success){
                    $success_pipelines[] = explode('-', $success)[0];
                    $success_statuses[] = explode('-', $success)[1];
                }
            }

            if(isset($ga['fail_status']) and !empty($ga['fail_status']) and is_array($ga['fail_status'])) {
                $fail_pipelines = array();
                $fail_statuses = array();
                foreach ($ga['fail_status'] as $fail) {
                    $fail_pipelines[] = explode('-', $fail)[0];
                    $fail_statuses[] = explode('-', $fail)[1];
                }
            }

            $ti = $input['leads']['status'][0]['id']; // lead id

            //set utm terms
            foreach ($input['leads']['status'][0]['custom_fields'] as $custom_field){
                if(isset($ga['clientid']) and !empty($ga['clientid']) and $ga['clientid'] == $custom_field['id']){
                    $gaClientId = $custom_field['values'][0]['value'];
                }
                if(isset($ga['source_filed']) and !empty($ga['source_filed']) and $ga['source_filed'] == $custom_field['id']){
                    $utm_source = $custom_field['values'][0]['value'];
                }
                if(isset($ga['utm_medium']) and !empty($ga['utm_medium']) and $ga['utm_medium'] == $custom_field['id']){
                    $utm_medium = $custom_field['values'][0]['value'];
                }
                if(isset($ga['utm_campaign']) and !empty($ga['utm_campaign']) and $ga['utm_campaign'] == $custom_field['id']){
                    $utm_campaign = $custom_field['values'][0]['value'];
                }
                if(isset($ga['utm_content']) and !empty($ga['utm_content']) and $ga['utm_content'] == $custom_field['id']){
                    $utm_content = $custom_field['values'][0]['value'];
                }
                if(isset($ga['utm_term']) and !empty($ga['utm_term']) and $ga['utm_term'] == $custom_field['id']){
                    $utm_term = $custom_field['values'][0]['value'];
                }
                if(isset($ga['gclid']) and !empty($ga['gclid']) and $ga['gclid'] == $custom_field['id']){
                    $gclid = $custom_field['values'][0]['value'];
                }
            }


            $user = User::first();
            $amo = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain);
            $lead = $amo->getLeadById($ti);

            file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] test'.print_r($lead, true).PHP_EOL, FILE_APPEND);
            die();



            if(!isset($gaClientId) or empty($gaClientId)){

            }


            if(isset($gaClientId) and !empty($gaClientId)){
                if(!empty($success_pipelines) and !empty($success_statuses) and @isset($input['leads']['status'][0]['pipeline_id']) and in_array($input['leads']['status'][0]['pipeline_id'], $success_pipelines)  and @isset($input['leads']['status'][0]['status_id']) and in_array($input['leads']['status'][0]['status_id'], $success_statuses)){ // integration for success statuses
                    file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] success status triggered'.PHP_EOL, FILE_APPEND);
                    $price = $input['leads']['status'][0]['price']; // lead price

                    if(isset($gclid) and !empty($gclid)){
                        file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] by gclid'.PHP_EOL, FILE_APPEND);
                        //integration by GCLID
                        $request = array(
                            'v' => 1,
                            't' => 'transaction',
                            'tid' => $ga['view_id'],
                            'cid' => $gaClientId,
                            'ti' => $ti,
                            'ta' => 'SITE',
                            'tr' => $price,
                            'cd'.$ga['cd_index'] => $ti,
                            'gclid' => $gclid
                        );
                    }else{
                        if(isset($utm_source) and !empty($utm_source) and isset($utm_medium) and !empty($utm_medium) and isset($utm_content) and !empty($utm_content) and isset($utm_term) and !empty($utm_term) and isset($utm_campaign) and !empty($utm_campaign) ){
                            file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] by utm'.PHP_EOL, FILE_APPEND);
                            //intehration by UTM
                            $request = array(
                                'v'=>1,
                                't'=>'transaction',
                                'tid'=>$ga['view_id'],
                                'cid'=>$gaClientId,
                                'ti'=>$ti,
                                'ta'=>'SITE',
                                'tr'=>$price,
                                'cd'.$ga['cd_index']=>$ti,
                                'cs' => $utm_source,
                                'cm' => $utm_medium,
                                'cc' => $utm_content,
                                'ck' => $utm_term,
                                'cn' => $utm_campaign
                            );
                        }else{
                            //integration by CLIENT ID
                            file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] by clientid'.PHP_EOL, FILE_APPEND);
                            $request = array(
                                'v' => 1,
                                't' => 'transaction',
                                'tid' => $ga['view_id'],
                                'cid' => $gaClientId,
                                'ti' => $ti,
                                'ta' => 'SITE',
                                'tr' => $price,
                                'cd'.$ga['cd_index'] => $ti
                            );
                        }
                    }
                    if(isset($request) and !empty($request)){
                        file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] request exist'.PHP_EOL, FILE_APPEND);
                        $ch = curl_init('http://www.google-analytics.com/collect');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));  //Post Fields
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $server_output = curl_exec ($ch);
                        curl_close ($ch);
                        file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] '.print_r($server_output, true).' '.PHP_EOL, FILE_APPEND);
                        return $server_output;
                    }
                }elseif( !empty($fail_pipelines) and !empty($fail_statuses) and @isset($input['leads']['status'][0]['pipeline_id']) and in_array($input['leads']['status'][0]['pipeline_id'], $fail_pipelines)  and @isset($input['leads']['status'][0]['status_id']) and in_array($input['leads']['status'][0]['status_id'], $fail_statuses)){ //integration for fail statuses
                    file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] fail status triggered'.PHP_EOL, FILE_APPEND);
                    if(isset($gclid) and !empty($gclid)){
                        //integration by GCLID
                        file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] by gclid'.PHP_EOL, FILE_APPEND);
                        $request = array(
                            'v'=>1,
                            't'=>'pageview',
                            'tid'=>$ga['view_id'],
                            'cid'=>$gaClientId,
                            'dh'=>$ga['site_url'],
                            'dp'=>'/',
                            'cm'.$ga['cm_index']=>1,
                            'cd'.$ga['cd_index']=>$ti,
                            'gclid' => $gclid
                        );
                    }else{
                        if(isset($utm_source) and !empty($utm_source) and isset($utm_medium) and !empty($utm_medium) and isset($utm_content) and !empty($utm_content) and isset($utm_term) and !empty($utm_term) and isset($utm_campaign) and !empty($utm_campaign) ){
                            //intehration by UTM
                            file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] by utm'.PHP_EOL, FILE_APPEND);
                            $request = array(
                                'v'=>1,
                                't'=>'pageview',
                                'tid'=>$ga['view_id'],
                                'cid'=>$gaClientId,
                                'dh'=>$ga['site_url'],
                                'dp'=>'/',
                                'cm'.$ga['cm_index']=>1,
                                'cd'.$ga['cd_index']=>$ti,
                                'cs' => $utm_source,
                                'cm' => $utm_medium,
                                'cc' => $utm_content,
                                'ck' => $utm_term,
                                'cn' => $utm_campaign
                            );
                        }else{
                            //integration by CLIENT ID
                            file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] by client id'.PHP_EOL, FILE_APPEND);
                            $request = array(
                                'v'=>1,
                                't'=>'pageview',
                                'tid'=>$ga['view_id'],
                                'cid'=>$gaClientId,
                                'dh'=>$ga['site_url'],
                                'dp'=>'/',
                                'cm'.$ga['cm_index']=>1,
                                'cd'.$ga['cd_index']=>$ti
                            );
                        }
                    }
                    if(isset($request) and !empty($request)){
                        $ch = curl_init('http://www.google-analytics.com/collect');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));  //Post Fields
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $server_output = curl_exec ($ch);
                        curl_close ($ch);
                        file_put_contents('ga_webhook.txt', '['.date('d-m-Y H:i:s').'] '.print_r($server_output, true).PHP_EOL, FILE_APPEND);
                        return $server_output;
                    }
                }
            }
        }
    }
}