<?php
/**
 * Created by PhpStorm.
 * User: molofeev
 * Date: 17.05.17
 * Time: 15:56
 */
Route::group(['namespace' => 'Modules\GA\Controllers'], function()
{
    Route::get('/ga', ['uses' => 'GAController@index']);
    Route::post('/ga/update', ['uses'=>'GAController@update']);
    Route::post('/ga/webhook', ['uses'=>'GAController@webhook']);
});