@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <style>
        .form-control{
            width:100%!important;
        }
    </style>
    <section id="main-content">
        <section class="wrapper">
            <form class="form-inline" role="form" method="POST" action="{{ url('/ga/update') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-6">
                        <h1>Интеграция с GA</h1>
                    </div>
                    <div class="col-xs-6">
                        <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 responsible-panel-place">
                        <section class="panel">
                            <header class="panel-heading">
                                Настройки статусов
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label>Успешный статус<a href="#" data-toggle="tooltip" data-placement="right" title="Укажите, по одному на воронку, статусы, при достижении которых будет передаватся информация о успешной сделке с amoCRM в GA"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select class="form-control success_status_select" multiple name="ga[success_status][]">
                                            @foreach($leads_statuses as $pipeline)
                                                <optgroup label="{{$pipeline['name']}}">
                                                    @foreach($pipeline['statuses'] as $status)
                                                        <option {{ ( isset($ga['success_status']) and in_array($pipeline['id'].'-'.$status['id'], $ga['success_status']) ) ? 'selected' : '' }} value="{{$pipeline['id'].'-'.$status['id']}}">{{$status['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>Неуспешный статус<a href="#" data-toggle="tooltip" data-placement="right" title="Укажите, по одному на воронку, статусы, при достижении которых будет передаватся информация о неуспешной сделке с amoCRM в GA"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select class="form-control fail_status_select" multiple name="ga[fail_status][]">
                                            @foreach($leads_statuses as $pipeline)
                                                <optgroup label="{{$pipeline['name']}}">
                                                    @foreach($pipeline['statuses'] as $status)
                                                        <option {{ ( isset($ga['fail_status']) and in_array($pipeline['id'].'-'.$status['id'], $ga['fail_status']) ) ? 'selected' : '' }} value="{{$pipeline['id'].'-'.$status['id']}}">{{$status['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="panel responsible-panel">
                            <header class="panel-heading">
                                Данные для связи с GA
                            </header>
                            <div class="panel-body">
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>UTM source</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[utm_source]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'leads')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['utm_source']) and $ga['utm_source'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>UTM medium</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[utm_medium]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'leads')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['utm_medium']) and $ga['utm_medium'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>UTM campaign</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[utm_campaign]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'leads')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['utm_campaign']) and $ga['utm_campaign'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>UTM content</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[utm_content]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'leads')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['utm_content']) and $ga['utm_content'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>UTM term</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[utm_term]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'leads')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['utm_term']) and $ga['utm_term'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        @if(isset($errors['utm']) and !empty($errors['utm']))
                                            <span class="text-danger">{{$errors['utm']}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>gclid для Google adwords</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[gclid]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'leads')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['gclid']) and $ga['gclid'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        @if(isset($errors['gclid']) and !empty($errors['gclid']))
                                            <span class="text-danger">{{$errors['gclid']}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Общие данные для связи аккаунтов
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label>Индекс пользовательского параметра для хранения id сделки<a href="#" data-toggle="tooltip" data-placement="right" title="Обязательный параметр, хранится в GA"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" name="ga[cd_index]" value="{{ (isset($ga['cd_index'])) ? $ga['cd_index'] : '' }}" class="form-control">
                                        @if(isset($errors['cd_index']) and !empty($errors['cd_index']))
                                            <span class="text-danger">{{$errors['cd_index']}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>Индекс пользовательского показателя для подсчета сделок<a href="#" data-toggle="tooltip" data-placement="right" title="Обязательный показатель для подсчета неуспешных сделок, хранится в GA"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" name="ga[cm_index]" value="{{ (isset($ga['cm_index'])) ? $ga['cm_index'] : '' }}" class="form-control">
                                        @if(isset($errors['cm_index']) and !empty($errors['cm_index']))
                                            <span class="text-danger">{{$errors['cm_index']}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>Адрес главной страницы сайта<a href="#" data-toggle="tooltip" data-placement="right" title="Обязательное поле для неуспешного статуса"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" name="ga[site_url]" value="{{ (isset($ga['site_url'])) ? $ga['site_url'] : '' }}" class="form-control">
                                        @if(isset($errors['site_url']) and !empty($errors['site_url']))
                                            <span class="text-danger">{{$errors['site_url']}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>Google client id<a href="#" data-toggle="tooltip" data-placement="right" title="Обязательное поле, если в сделке не заполнено, интеграция не выполнится"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[clientid]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'leads')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['clientid']) and $ga['clientid'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        @if(isset($errors['clientid']) and !empty($errors['clientid']))
                                            <span class="text-danger">{{$errors['clientid']}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>Google client id в контакте<a href="#" data-toggle="tooltip" data-placement="right" title="Необязательный параметр, используется для поиска client id  в контакте если в сделке он отсутствует"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="ga[contact_clientid]" class="form-control">
                                            <option value="">Выберите поле</option>
                                            @foreach($amo_fields as $key => $fields)
                                                @continue($key != 'contacts')
                                                <optgroup label="{{$key}}">
                                                    @foreach($fields as $field)
                                                        <option {{ (isset($ga['contact_clientid']) and $ga['contact_clientid'] == $field['id']) ? 'selected' : '' }} value="{{$field['id']}}">{{$field['name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row top_buffer">
                                    <div class="col-xs-6">
                                        <label>Трэкинг ID. В формате UA-XXXX-Y<a href="#" data-toggle="tooltip" data-placement="right" title="Обязательное поле, хранится в GA"><i class="fa fa-info-circle"></i></a></label>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" name="ga[view_id]" value="{{ (isset($ga['view_id'])) ? $ga['view_id'] : '' }}" class="form-control">
                                        @if(isset($errors['view_id']) and !empty($errors['view_id']))
                                            <span class="text-danger">{{$errors['view_id']}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 responsible-panel-place2">

                    </div>
                </div>
            </form>
        </section>
    </section>
    <!--main content end-->
@endsection