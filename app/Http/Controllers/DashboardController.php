<?php

namespace App\Http\Controllers;
require_once app_path().'/Amo/map.php';
use App\Module;
use App\User;
use Chumper\Zipper\Facades\Zipper;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Route;
use Session;
use Mail;

class DashboardController extends Controller
{

    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->middleware('auth');
        $this->middleware('amo')->only('show');
        $this->request = $request;
    }

    public function show(){
        $user = Auth::user();
        $amo = \AmoCRM::get_instance($user->amo_email, $user->amo_hash, $user->amo_domain );
        $amo_data = $amo->getAccountInfo();
        return view('account.new.dashboard', ['user'=>$user, 'amo_data'=>$amo_data]);
    }

    public function settings()
    {
        $user = Auth::user();
        return view('account.new.settings', ['user'=>$user]);
    }

    public function settings_update()
    {
        $user = Auth::user();
        $input = $this->request->all();
        $this->validate($this->request, [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'password' => 'min:6|confirmed',
            'amo_domain' => 'required',
            'amo_email' => 'required|email',
            'amo_hash' =>  'required|min:32'
        ]);
        if($input['password']!=''){
            $user->password = bcrypt($input['password']);
        }
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->amo_domain = $input['amo_domain'];
        $user->amo_email = $input['amo_email'];
        $user->amo_hash = $input['amo_hash'];
        $user->save();
        return redirect('settings');
    }

    public function users()
    {
        $users = User::all();
        $user = Auth::user();
        return view('account.new.users', ['users'=>$users, 'user'=>$user]);
    }

    public function users_new()
    {
        $input = $this->request->all();
        $this->validate($this->request, [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'role' => 'in:3,4'
        ]);
        $password = str_random(8);
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => bcrypt($password)
        ]);
        $user->save();
        $user->roles()->attach($input['role']);

        Mail::raw('New user of subdomain created. User name: '.$user->name.' User email: '.$user->email, function($message)
        {
            $message->from('platform@fabrika-klientov.com', 'Platform');

            $message->to('php.am@fabrika-klientov.com');
        });

        Mail::raw('Привет в Платформе Ваш пароль '.$password. ' Ваш логин/Почта '. $user->email, function($message) use ($user)
        {
            $message->from('platform@fabrika-klientov.com', 'Platform');

            $message->to($user->email);
        });
        return redirect('/users');


    }

    public function user_control($id)
    {
        $input = $this->request->all();
        $user = User::where('id', '=', $id)->first();
        if(!isset($user)){
            return view('errors.fail', ['errormsg' => 'Такого пользователя не существует', 'link' => '/users']);
        }
        if (($user->hasRole('viewer') or $user->hasRole('editor'))  and isset($input['action'])) {
            switch ($input['action']) {
                case 'edit':
                    return view('account.new.edit_user', ['user'=>Auth::user(), 'editing_user'=>$user]);
                    break;
                case 'remove':
                    $user->delete();
                    return redirect('users');
                    break;
                case 'save':
                    $this->validate($this->request, [
                        'name' => 'required',
                        'email' => 'required|email|max:255|unique:users,email,'.$user->id,
                        'password' => 'min:6|confirmed',
                        'role' => 'in:3,4'
                    ]);
                    if($input['password']!=''){
                        $user->password = bcrypt($input['password']);
                    }
                    $user->name = $input['name'];
                    $user->email = $input['email'];
                    $user->save();
                    $user->roles()->detach();
                    $user->roles()->attach($input['role']);
                    return redirect('users');
                    break;
            }
        }else{
            return redirect('users');
        }
    }

    public function modules(){
        $modules = array();
        foreach (scandir(app_path('Http/Controllers/Modules/')) as $item){
            if ('.' === $item) continue;
            if ('..' === $item) continue;
            if( is_dir(app_path('Http/Controllers/Modules/'.$item)) and file_exists( app_path('Http/Controllers/Modules/'.$item.'/Controllers/MainController.php') ) ){
                $modules[] = array('title' => $item, 'status' => (Module::where('name', '=', $item)->first()) !== null ? 'on' : 'off');
            }
        }
        return view('account.new.modules', array('modules'=>$modules));
    }

    public function moduleUpload(){
        $file = array('arch' => Input::file('arch'));
        $rules = array('arch' => 'required|mimes:zip');
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            return view('errors.fail', ['errormsg' => $validator->messages()->messages()['arch'][0], 'link' => '/modules']);
        }else {
            if (Input::file('arch')->isValid()) {
                $destinationPath = app_path().'/Http/Controllers/Modules';
                $fileName = Input::file('arch')->getClientOriginalName();
                if(file_exists($destinationPath.'/'.$fileName)){
                    return view('errors.fail', ['errormsg' => 'Такой файл уже существует', 'link' => '/modules']);
                }
                Input::file('arch')->move($destinationPath, $fileName);
                Zipper::make($destinationPath.'/'.$fileName)->folder(explode('.zip', $fileName)[0])->extractTo($destinationPath.'/'.explode('.zip', $fileName)[0]);
                return redirect('/modules');
            }else {
                return view('errors.fail', ['errormsg' => 'Загружаемый файл не валидный', 'link' => '/modules']);
            }
        }
    }
}