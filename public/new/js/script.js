/**
 * Created by molofeev on 23.03.17.
 */
$(document).ready(function(){

    $('[data-confirm=true]').click(function(e){
        var conf = confirm("Подтвердите действие");
        if (conf) {
            return true;
        }else{
            e.preventDefault();
            return false;
        }

    });

});