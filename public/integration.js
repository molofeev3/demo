var INTEGRATION = INTEGRATION || (function(){
        var _args = {};
        var terms = {};
        var QueryString = function () {
            // This function is anonymous, is executed immediately and
            // the return value is assigned to QueryString!
            var query_string = {};
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                // If first entry with this name
                if (typeof query_string[pair[0]] === "undefined") {
                    query_string[pair[0]] = decodeURIComponent(pair[1]);
                    // If second entry with this name
                } else if (typeof query_string[pair[0]] === "string") {
                    var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
                    query_string[pair[0]] = arr;
                    // If third or later entry with this name
                } else {
                    query_string[pair[0]].push(decodeURIComponent(pair[1]));
                }
            }
            return query_string;
        }();

        return {
            init : function(Args) {
                _args = Args;
                terms = QueryString;
            },
            integrate : function() {
                for(var i=0; i<document.forms.length; i++){
                    var form = document.forms[i];
                    form.addEventListener("submit", function(){
                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                console.log(this.responseText);
                            }
                        };
                        var kvpairs = [];
                        for ( var i = 0; i < this.elements.length; i++ ) {
                            var e = this.elements[i];
                            kvpairs.push(encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value));
                        }
                        var queryString = kvpairs.join("&");
                        var queryTerms = '';
                        if(typeof(terms.utm_source) != "undefined" && terms.utm_source != '' ){
                            queryTerms += '&utm_source='+terms.utm_source;
                        }
                        if(typeof(terms.utm_medium) != "undefined" && terms.utm_medium != '' ){
                            queryTerms += '&utm_medium='+terms.utm_medium;
                        }
                        if(typeof(terms.utm_term) != "undefined" && terms.utm_term != '' ){
                            queryTerms += '&utm_term='+terms.utm_term;
                        }
                        if(typeof(terms.utm_content) != "undefined" && terms.utm_content != '' ){
                            queryTerms += '&utm_content='+terms.utm_content;
                        }
                        if(typeof(terms.utm_campaign) != "undefined" && terms.utm_campaign != '' ){
                            queryTerms += '&utm_campaign='+terms.utm_campaign;
                        }
                        xhttp.open("GET", 'http://platform.com/amo/integration/'+_args[0]+'?'+queryString+queryTerms, false);
                        xhttp.send();
                    }, false);
                }
            }
        };
    }());