$(document).ready(function () {

    if($('[data-toggle="tooltip"]').length){
        $('[data-toggle="tooltip"]').tooltip();
    }

    $('.success_status_select, .fail_status_select').select2({
        placeholder: 'Выберите статус…'
    });

    $(window).resize(function(){
        if($(window).width() < 992){
            $('.responsible-panel-place2').append($('.responsible-panel'));
        }else{
            $('.responsible-panel-place').append($('.responsible-panel'));
        }
    });

    if($(window).width() < 992){
        $('.responsible-panel-place2').append($('.responsible-panel'));
    }else{
        $('.responsible-panel-place').append($('.responsible-panel'));
    }
});