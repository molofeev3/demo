$(document).ready(function () {
    $(".js-example-basic-multiple").select2({
        placeholder: 'Выберите сотрудника…'
    });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        var init = new Switchery(html, { color: 'rgb(100, 189, 99)', jackColor: '#fff' });
    });

});

$(document).ready(function() {
    needToConfirm = false;
    window.onbeforeunload = askConfirm;
});


function askConfirm() {
    if (needToConfirm) {
        return "Есть не сохраненные данные на странице, покинуть?";
    }
}

$('body').on('change', 'select,input,textarea', function() {
    console.log('changed');
    needToConfirm = true;
});
$('body').on('submit', 'form', function(){
    window.onbeforeunload = null;
});
$('body').on('click', '.del-site-link', function(){
    window.onbeforeunload = null;
});


$(document).ready(function(){
    $(".site-name").hover(
        function () {
            $('.site-name-pensil').show();
        },
        function () {
            $('.site-name-pensil').hide();
        }
    );

    $('input[name="tasks"]').change(function(){
        if($(this).prop('checked')){
            $('.task-panel .row').show();
            $('.advanced-panel').show();

        }else{
            $('.task-panel .row:not(.tasks_status)').hide();
            $('.advanced-panel').hide();
        }
    });
    if($('input[name="tasks"]').prop('checked')){
        $('.task-panel .row').show();
        $('.advanced-panel').show();

    }else{
        $('.task-panel .row:not(.tasks_status)').hide();
        $('.advanced-panel').hide();
    }

    //schedule
    $('body').on('click', '.check_row', function(){
        var self = $(this);
        var cb = $(this).parents('tr').find('input[type=checkbox]:not(.check_row)');
        cb.each(function(){
            if(self.prop('checked')){
                $(this).parents('label').removeClass('c_off').addClass('c_on');
                $(this).prop('checked', true);
            }else{
                if($(this).parents('table').find('tr:last td:eq('+$(this).parents('td').index()+') .check_col').prop('checked') == false ){
                    $(this).prop('checked', false);
                    $(this).parents('label').removeClass('c_on').addClass('c_off');
                }
            }
        });
    });

    $('body').on('click', '.check_col', function(){
        var self = $(this);
        self.parents('table').find('tr:not(:last)').each(function(){
            var cb = $(this).find('td:eq('+self.parent().index()+') input[type=checkbox]');
            if(self.prop('checked')){
                cb.parents('label').removeClass('c_off').addClass('c_on');
                cb.prop('checked', true);
            }else{
                if($(this).find('.check_row').prop('checked')==false){
                    cb.parents('label').removeClass('c_on').addClass('c_off');
                    cb.prop('checked', false);
                }
            }
        });
    });

    $('body').on('click', '.show_lead_fields', function(){
        $('.lead_fields').show();
        $(this).hide();
        $('.hide_lead_fields').show();
    });
    $('body').on('click', '.hide_lead_fields', function(){
        $('.lead_fields').hide();
        $(this).hide();
        $('.show_lead_fields').show();
        $('.lead_fields_site:not(:first)').parents('.field_param').remove();
        $('.lead_fields_amo:not(:first)').parents('.field_param').remove();
        $('.lead_fields_site option:first').prop('selected', true);
        $('.lead_fields_amo option:first').prop('selected', true);
    });

    $('body').on('click', '.clear_all', function () {
        $(this).parent().find('table input[type=checkbox]').prop('checked', false);
        $(this).parent().find('table label').removeClass('c_on').addClass('c_off');
    });

    $('body').on('click', '.check_all', function () {
        $(this).parent().find('table input[type=checkbox]').prop('checked', true);
        $(this).parent().find('table label').removeClass('c_off').addClass('c_on');
    });

    $('body').on('click', '.work8', function () {
        $(this).parent().find('table input[type=checkbox]').prop('checked', false);
        $(this).parent().find('table label').removeClass('c_on').addClass('c_off');
        $(this).parent().find('table tr:lt(5)').each(function(){
            var td = $(this).find('td').filter(function(i) {
                return i > 9 && i < 19 && i != 14
            });
            td.find('input[type=checkbox]').prop('checked', true);
            td.find('label').removeClass('c_off').addClass('c_on');
        });
    });

    $('body').on('click', '.work9', function () {
        $(this).parent().find('table input[type=checkbox]').prop('checked', false);
        $(this).parent().find('table label').removeClass('c_on').addClass('c_off');
        $(this).parent().find('table tr:lt(5)').each(function(){
            var td = $(this).find('td').filter(function(i) {
                return i > 10 && i < 20 && i != 15
            });
            td.find('input[type=checkbox]').prop('checked', true);
            td.find('label').removeClass('c_off').addClass('c_on');
        });
    });
    //end schedule

    //employers select
    function checkSelect(){
        if($('.js-example-basic-multiple').val()==null){
            $('.employer_panel').hide();
            return false;
        }
        $('.employer_panel').hide();
        $.each($('.js-example-basic-multiple').val(), function(i, e){
            var panel = $('#employer-'+e).parents('.employer_panel');
            panel.show();
            panel.parent().prepend(panel);
        });
    }

    checkSelect();

    $('body').on('change', '.js-example-basic-multiple', function(){
        checkSelect();
    });

    function selectEmployers(data){
        if(data.length != 0){
            $('.employer_panel').hide();
            $('.js-example-basic-multiple').val('');
            data.forEach(function(item){
                $('.js-example-basic-multiple option[value=' + item + ']').prop('selected', true);
                var panel = $('#employer-'+item).parents('.employer_panel');
                panel.show();
                panel.parent().prepend(panel);
            });
            $('.js-example-basic-multiple').select2({
                placeholder: 'Выберите сотрудника…'
            });
        }
    }

    $('body').on('click', '.active-employers', function(){
        var self = $(this);
        $.ajax({
            type: 'post',
            url: '/site_integration/employers/active/'+location.href.match(/([^\/]*)\/*$/)[1],
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                console.log(data);
                self.removeClass('btn-default').addClass('btn-white');
                $('.disactive-employers, .all-employers').removeClass('btn-white').addClass('btn-default');
                selectEmployers(data.schedule);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('body').on('click', '.disactive-employers', function(){
        var self = $(this);
        $.ajax({
            type: 'post',
            url: '/site_integration/employers/disactive/'+location.href.match(/([^\/]*)\/*$/)[1],
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                console.log(data);
                self.removeClass('btn-default').addClass('btn-white');
                $('.active-employers, .all-employers').removeClass('btn-white').addClass('btn-default');
                selectEmployers(data.schedule);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('body').on('click', '.all-employers', function(){
        var self = $(this);
        $.ajax({
            type: 'post',
            url: '/site_integration/employers/all/'+location.href.match(/([^\/]*)\/*$/)[1],
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                console.log(data);
                self.removeClass('btn-default').addClass('btn-white');
                $('.disactive-employers, .active-employers').removeClass('btn-white').addClass('btn-default');
                selectEmployers(data.schedule);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
    //end employers select


    //styled checkbox start
    function chechCheckbox(){
        $('.label_check').each(function(){
            var self = $(this);
            if(self.find('input[type=checkbox]').prop('checked')){
                self.removeClass('c_off').addClass('c_on');
            }else{
                self.removeClass('c_on').addClass('c_off');
            }
        });
    }
    chechCheckbox();
    $('body').on('click', '.label_check', function(){
        var self = $(this);
        if(self.find('input[type=checkbox]').prop('checked')){
            self.find('input[type=checkbox]').prop('checked', false);
        }else{
            self.find('input[type=checkbox]').prop('checked', true);
        }
        chechCheckbox();
    });
    //styled checkbox end

    $('[data-toggle="tooltip"]').tooltip();

    $('body').on('click', '.add_field_param', function(){
        var element = $(this).parents('.field_group').find('.field_param:last');
        var clone = element.clone();
        clone.find('.lead_fields_site option:first').prop('selected', true);
        clone.find('.lead_fields_amo option:first').prop('selected', true);
        clone.find('input').val('');
        clone.insertAfter(element);
    });
    $('body').on('click', '.del_field_param', function(){
        if($(this).parents('.field_group').find('.field_param').length > 1){
            $(this).parents('.field_param').remove();
        }
    });


});