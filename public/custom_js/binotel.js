$(document).ready(function(){

    $('body').on('change', '.utm_step_1 select', function(){
        if($(this).val() != '0'){
            $('.utm_step_2').removeClass('hidden');
            $('.utm_step_3 select optgroup').show();
            $('.utm_step_3 select').val('0');
            $('.utm_step_3 select').each(function () {
                $(this).find('optgroup[label!="'+$('.utm_step_1 select').val()+'"]').hide();
            });
        }else{
            $('.utm_step_2').addClass('hidden');
            $('.utm_step_3').addClass('hidden');
        }
    });
    $('body').on('change', '.utm_step_2 select', function(){
        if($(this).val() == 'fields'){
            $('.utm_step_3').removeClass('hidden');
            $('.utm_step_3 select').each(function () {
                $(this).find('optgroup[label!="'+$('.utm_step_1 select').val()+'"]').hide();
            });
        }else{
            $('.utm_step_3').addClass('hidden');
            $('.utm_step_3 select optgroup').show();
        }
    });
    utmStep1();
    function utmStep1() {
        if($('.utm_step_1 select').val() != '0'){
            $('.utm_step_2').removeClass('hidden');
            $('.utm_step_3 select optgroup').show();
            $('.utm_step_3 select').each(function () {
                $(this).find('optgroup[label!="'+$('.utm_step_1 select').val()+'"]').hide();
            });
        }else{
            $('.utm_step_2').addClass('hidden');
            $('.utm_step_3').addClass('hidden');
        }
    }
    utmStep2();
    function utmStep2() {
        if($('.utm_step_2 select').val() == 'fields'){
            $('.utm_step_3').removeClass('hidden');
            $('.utm_step_3 select').each(function () {
                $(this).find('optgroup[label!="'+$('.utm_step_1 select').val()+'"]').hide();
            });
        }else{
            $('.utm_step_3').addClass('hidden');
            $('.utm_step_3 select optgroup').show();
        }
    }

    ///binotel module
    $('body').on('click', '.add_phone', function(){
        var element = $(this).prev().find('tbody tr:last');
        var clone = element.clone();
        clone.find('input').val('');
        clone.find('select option:first').prop('selected', true);
        clone.insertAfter(element);
    });

    $('body').on('click', '.del_phone', function(){
        if($(this).parents('tbody').find('tr').length > 1){
            $(this).parents('tr').remove();
        }
    });

    $('.case-0').hide();
    $('.case-0-'+$('[name="case[input][answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[input][answer][no_contact][value]"]', function(){
        $('.case-0').hide();
        $('.case-0-'+$(this).val()).show();
    });

    $('.case-1').hide();
    $('.case-1-'+$('[name="case[input][no_answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[input][no_answer][no_contact][value]"]', function(){
        $('.case-1').hide();
        $('.case-1-'+$(this).val()).show();
    });
    /**/
    $('.case-ct').hide();
    $('.case-ct-'+$('[name="case[calltracking][answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[calltracking][answer][no_contact][value]"]', function(){
        $('.case-ct').hide();
        $('.case-ct-'+$(this).val()).show();
    });

    $('.case-ct1').hide();
    $('.case-ct1-'+$('[name="case[calltracking][no_answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[calltracking][no_answer][no_contact][value]"]', function(){
        $('.case-ct1').hide();
        $('.case-ct1-'+$(this).val()).show();
    });
    /**/

    $('.case-2').hide();
    $('.case-2-'+$('[name="case[output][answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[output][answer][no_contact][value]"]', function(){
        $('.case-2').hide();
        $('.case-2-'+$(this).val()).show();
    });

    $('.case-3').hide();
    $('.case-3-'+$('[name="case[output][no_answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[output][no_answer][no_contact][value]"]', function() {
        $('.case-3').hide();
        $('.case-3-' + $(this).val()).show();
    });

    $('.case-4').hide();
    $('.case-4-'+$('[name="case[getcall][answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[getcall][answer][no_contact][value]"]', function() {
        $('.case-4').hide();
        $('.case-4-' + $(this).val()).show();
    });

    $('.case-5').hide();
    $('.case-5-'+$('[name="case[getcall][no_answer][no_contact][value]"] option:selected').val()).show();

    $('body').on('change', '[name="case[getcall][no_answer][no_contact][value]"]', function() {
        $('.case-5').hide();
        $('.case-5-' + $(this).val()).show();
    });
});