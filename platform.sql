-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 28, 2016 at 12:28 PM
-- Server version: 5.5.53-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `platform`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_11_09_080737_add_amo_column_to_users', 1),
('2016_11_23_093922_add_domain_id_column_to_user', 2),
('2016_11_28_110001_create_sessions_table', 3),
('2016_11_28_133604_add_roles_table', 4),
('2016_11_28_133730_add_user_roles_table', 4),
('2016_12_06_115040_remove_column_for_amo_add_for_email_confirm', 5),
('2016_12_19_132953_add_settings_column_to_user', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'viewer'),
(4, 'editor');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(1, 8, 2),
(2, 9, 2),
(3, 10, 3),
(4, 11, 3),
(5, 12, 3),
(6, 13, 3),
(7, 14, 3),
(8, 15, 3),
(9, 16, 3),
(10, 17, 3),
(11, 18, 3),
(12, 19, 3),
(13, 20, 3),
(14, 21, 3),
(15, 22, 3),
(16, 23, 3),
(17, 24, 3),
(18, 25, 3),
(19, 26, 3),
(20, 27, 3),
(21, 28, 3),
(24, 31, 2),
(25, 32, 2),
(26, 33, 2),
(27, 34, 2),
(28, 35, 2),
(29, 36, 2),
(30, 37, 2),
(31, 38, 2),
(32, 39, 2),
(33, 40, 2),
(34, 41, 2),
(35, 42, 2),
(36, 43, 3),
(42, 45, 3),
(43, 44, 4),
(44, 46, 2),
(45, 47, 4),
(46, 48, 4),
(47, 49, 4),
(48, 50, 3),
(49, 51, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('944303f93d403f25c1be0c065c60ab5b87b1f93e', NULL, '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoibGt6aFRCTWpIQWx5UGZacEFic2FiUzNJbkhXcnVGY2xSQnUzeWJCaCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozNToiaHR0cDovLzAwNDIucGxhdGZvcm0uY29tOjgwMDAvdXNlcnMiO31zOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czozNToiaHR0cDovLzAwNDIucGxhdGZvcm0uY29tOjgwMDAvdXNlcnMiO31zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ4MjE1NzEwMTtzOjE6ImMiO2k6MTQ4MjE1NzEwMTtzOjE6ImwiO3M6MToiMCI7fXM6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1482157101),
('b5a6414d9b860c790dd6d427a71d848d1d52284a', 42, '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiUEpSM2thaGtjTHZOd21KbTRQSGtyQmtpVnFBbE02clByNjl3V1VzOCI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjM4OiJodHRwOi8vMDA0Mi5wbGF0Zm9ybS5jb206ODAwMC9zZXR0aW5ncyI7fXM6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6NDI7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0ODIxNTg0NTQ7czoxOiJjIjtpOjE0ODIxNDc4NjY7czoxOiJsIjtzOjE6IjAiO319', 1482158454),
('f6dcda84c65a574ea994517ed4b4db554bcf6831', NULL, '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiOVVLOVdUQUJFVDVHYzRqOTdmdEhDNENwaHpXUHdrUlRMVTZzMFJueSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzU6Imh0dHA6Ly8wMDQyLnBsYXRmb3JtLmNvbTo4MDAwL2xvZ2luIjt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0ODIxNTcxMDE7czoxOiJjIjtpOjE0ODIxNTcxMDE7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1482157101);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `domain_id` int(4) unsigned zerofill NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amo_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amo_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amo_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `domain_id`, `confirmed`, `confirmation_code`, `amo_email`, `amo_domain`, `amo_hash`) VALUES
(42, 'admin', 'php.am@fabrika-klientov.com', '$2y$10$SEXL9AVp0fObTwAM5XQvoOJoBlPH/YKkRFug.zOCcJtt02QrhdkLC', 'D6bf2aB1PHvIpIzq09WkKKp1QHYZbAVYoMcQUBsX4UOL4t90HmZlaoOTF2NJ', '2016-12-06 10:36:36', '2016-12-19 12:40:34', 0042, 1, NULL, 'php.am@fabrika-klientov.com', 'prezent', '50d5a827275890ab8ee44eb3ea466ab6'),
(50, 'vova', 'php.vb@fabrika-klientov.com', '$2y$10$7jXxetV5uhZyPp/UbX7hKOrWAIeYXfCRBoLfnl9RLojvupYHBjIgG', NULL, '2016-12-06 13:22:56', '2016-12-06 13:22:56', 0042, 0, NULL, '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
