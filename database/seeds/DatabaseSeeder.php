<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    public function run()
    {
        $this->call('RoleTableSeeder');
        $this->call('MenuTableSeeder');
    }

}

class RoleTableSeeder extends Seeder {

    public function run()
    {
        \App\Role::create(array('name' => 'admin'));
        \App\Role::create(array('name' => 'user'));
        \App\Role::create(array('name' => 'editor'));
        \App\Role::create(array('name' => 'viewer'));
    }

}

class MenuTableSeeder extends Seeder {

    public function run()
    {
        \App\Menu::create(array('id' => 1, 'title' => 'Панель управления', 'url' => '/', 'ico' => 'fa-dashboard'));
        \App\Menu::create(array('id' => 2, 'title' => 'Пользователи', 'url' => 'users', 'ico' => 'fa-group'));
        \App\Menu::create(array('id' => 3, 'title' => 'Модули', 'ico' => 'fa-puzzle-piece'));
        \App\Menu::create(array('id' => 4, 'title' => 'Все модули', 'url' => 'modules', 'parent' => 3));
        \App\Menu::create(array('id' => 5, 'title' => 'Настройки', 'url' => 'settings', 'ico' => 'fa-gears'));
    }

}
