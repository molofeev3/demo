<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <title>Platform</title>

    <!-- Bootstrap core CSS -->
    <link href="{{env('APP_URL')}}/new/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/new/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{env('APP_URL')}}/new/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{env('APP_URL')}}/new/css/style.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/new/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{env('APP_URL')}}/new/js/html5shiv.js"></script>
    <script src="{{env('APP_URL')}}/new/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <form class="form-signin" method="POST" action="{{ url('/init') }}">
        <h2 class="form-signin-heading">Установка</h2>
        <div class="login-wrap">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input id="name" type="text" class="form-control" placeholder="Имя" name="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" placeholder="E-Mail адресс" name="email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" placeholder="Пароль" class="form-control" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" placeholder="Подтверждение пароля" class="form-control" name="password_confirmation">
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <button class="btn btn-lg btn-login btn-block" type="submit">Установить</button>

        </div>

    </form>

</div>



<!-- js placed at the end of the document so the pages load faster -->
<script src="{{env('APP_URL')}}/new/js/jquery.js"></script>
<script src="{{env('APP_URL')}}/new/js/bootstrap.min.js"></script>


</body>
</html>

