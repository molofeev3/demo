@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-6">
                <section class="panel">
                    <div class="bio-graph-heading">
                        Общая информация о аккаунте amoCRM
                    </div>
                    <div class="panel-body bio-graph-info">
                        <h1>Данные</h1>
                        <div class="row">
                            <div class="bio-row">
                                <p><span>ID аккаунта</span>: {{$amo_data['account']['id']}}</p>
                            </div>
                            <div class="bio-row">
                                <p><span>Назание аккаунта </span>: {{$amo_data['account']['name']}}</p>
                            </div>
                            <div class="bio-row">
                                <p><span>Уникальный субдомен </span>: {{$amo_data['account']['subdomain']}}</p>
                            </div>
                            <div class="bio-row">
                                <p><span>Оплачен от</span>: {{date('d/m/Y H:i:s', $amo_data['account']['paid_from'])}}</p>
                            </div>
                            <div class="bio-row">
                                <p><span>Оплачен до </span>: {{date('d/m/Y H:i:s', $amo_data['account']['paid_till'])}}</p>
                            </div>
                            <div class="bio-row">
                                <p><span>Email </span>: {{$user->email}}</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-6">
                <section class="panel">
                    <header class="panel-heading">
                        Striped Table
                    </header>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>ФИО</th>
                            <th>Логин</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($amo_data['account']['users'] as $key => $manager )
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$manager['id']}}</td>
                                <td>{{$manager['name']}}</td>
                                <td>{{$manager['login']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->
@endsection
