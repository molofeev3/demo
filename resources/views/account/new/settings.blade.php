@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h1> Настройки</h1>
            <div class="row">
                <div class="col-sm-5">
                    <section class="panel">
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/settings/update') }}">
                                <h2>Настройки аккаунта</h2>
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-2 control-label">Имя</label>
                                    <div class="col-md-10">
                                        <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-2 control-label">E-Mail Aдресс</label>
                                    <div class="col-md-10">
                                        <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-2 control-label">Пароль</label>
                                    <div class="col-md-10">
                                        <input id="password" type="password" class="form-control" name="password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="password-confirm" class="col-md-2 control-label">Подтверждение пароля</label>
                                    <div class="col-md-10">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <h2>Настройки подключения к amoCRM</h2>
                                <div class="form-group{{ $errors->has('amo_email') ? ' has-error' : '' }}">
                                    <label for="amo_email" class="col-md-2 control-label">E-Mail Aдресс</label>
                                    <div class="col-md-10">
                                        <input id="amo_email" type="email" class="form-control" name="amo_email" value="{{$user->amo_email}}">
                                        @if ($errors->has('amo_email'))
                                            <span class="help-block">
                            <strong>{{ $errors->first('amo_email') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('amo_domain') ? ' has-error' : '' }}">
                                    <label for="amo_domain" class="col-md-2 control-label">Домен</label>
                                    <div class="col-md-10">
                                        <input id="amo_domain" type="text" class="form-control" name="amo_domain" value="{{$user->amo_domain}}">

                                        @if ($errors->has('amo_domain'))
                                            <span class="help-block">
                            <strong>{{ $errors->first('amo_domain') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('amo_hash') ? ' has-error' : '' }}">
                                    <label for="amo_hash" class="col-md-2 control-label">API ключ</label>
                                    <div class="col-md-10">
                                        <input id="amo_hash" type="text" class="form-control" name="amo_hash" value="{{$user->amo_hash}}">

                                        @if ($errors->has('amo_hash'))
                                            <span class="help-block">
                            <strong>{{ $errors->first('amo_hash') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-2 col-sm-offset-2">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-user"></i> Сохранить
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->
@endsection
