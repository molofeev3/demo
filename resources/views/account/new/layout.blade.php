<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Platform</title>

    <!-- Bootstrap core CSS -->
    <link href="{{env('APP_URL')}}/new/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/new/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{env('APP_URL')}}/new/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="{{env('APP_URL')}}/new/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{{env('APP_URL')}}/new/css/owl.carousel.css" type="text/css">

    <link rel="stylesheet" type="text/css" href="{{env('APP_URL')}}/new/assets/switchery/switchery.css" />
    <link rel="stylesheet" type="text/css" href="{{env('APP_URL')}}/new/assets/select2/css/select2.min.css"/>
    <!--right slidebar-->
    <link href="{{env('APP_URL')}}/new/css/slidebars.css" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="{{env('APP_URL')}}/new/css/style.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/new/css/custom.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/new/css/style-responsive.css" rel="stylesheet" />



    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{env('APP_URL')}}/new/js/html5shiv.js"></script>
    <script src="{{env('APP_URL')}}/new/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="has-js">

<section id="container">
    <!--header start-->
    <header class="header blue-bg" >
        <div class="sidebar-toggle-box">
            <i class="fa fa-bars"></i>
        </div>
        <!--logo start-->
        <a href="{{env('APP_URL')}}" class="logo">Platform</a>
        <!--logo end-->
        </div>
        <div class="top-nav ">
            <!--search & user info start-->
            <ul class="nav pull-right top-menu">
                <li class="account-id-wrapper"></li>
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="username">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-key"></i> Выйти</a></li>
                    </ul>
                </li>

                <!-- user login dropdown end -->
            </ul>
            <!--search & user info end-->
        </div>
    </header>
    <!--header end-->



@yield('content')

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="{{env('APP_URL')}}/new/js/jquery.js"></script>
<script src="{{env('APP_URL')}}/new/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="{{env('APP_URL')}}/new/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="{{env('APP_URL')}}/new/js/jquery.scrollTo.min.js"></script>
<script src="{{env('APP_URL')}}/new/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="{{env('APP_URL')}}/new/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="{{env('APP_URL')}}/new/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="{{env('APP_URL')}}/new/js/owl.carousel.js" ></script>
<script src="{{env('APP_URL')}}/new/js/jquery.customSelect.min.js" ></script>
<script src="{{env('APP_URL')}}/new/js/respond.min.js" ></script>


<!--bootstrap-switch-->
<script src="{{env('APP_URL')}}/new/assets/switchery/switchery.js"></script>
<!--right slidebar-->
<script src="{{env('APP_URL')}}/new/js/slidebars.min.js"></script>

<!--common script for all pages-->
<script src="{{env('APP_URL')}}/new/js/common-scripts.js"></script>

<!--script for this page-->
<script src="{{env('APP_URL')}}/new/js/sparkline-chart.js"></script>
<script src="{{env('APP_URL')}}/new/js/easy-pie-chart.js"></script>
<script src="{{env('APP_URL')}}/new/js/count.js"></script>
<script src="{{env('APP_URL')}}/new/js/script.js"></script>





<script type="text/javascript" src="{{env('APP_URL')}}/new/assets/select2/js/select2.min.js"></script>
{{--<script src="{{env('APP_URL')}}/new/js/form-component.js"></script>--}}
{{--<script type="text/javascript" src="{{env('APP_URL')}}/new/js/ga.js"></script>--}}

<script>

    //owl carousel

    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });

        $('ul.sub').each(function(){
            if($(this).find('li.active').length != 0){
                $(this).show();
            }
        });

    });

    //custom select box

    $(function(){
        $('select.styled').customSelect();
    });

</script>

<?php
foreach (scandir(base_path('public/custom_js/')) as $item){
    if ('.' === $item) continue;
    if ('..' === $item) continue;
    if(!is_dir($item)){
        ?>
        <script type="application/javascript" src="{{ URL::asset('custom_js/'.$item) }}"></script>
        <?php
    }
}
?>
</body>
</html>
