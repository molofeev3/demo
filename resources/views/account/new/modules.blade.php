@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-xs-8">
                    <h1> Модули</h1>
                </div>
                <div class="col-xs-1 col-xs-offset-3">
                    <a href="#upload-modal" data-toggle="modal" class="btn btn-primary top_buffer">Загрузить</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Доступные модули
                        </header>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Статус</th>
                                        <th>Действие</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($modules as $module)
                                        <tr>
                                            <td>{{$module['title']}}</td>
                                            <td>
                                                @if($module['status'] == 'on')
                                                    <span class="label label-success label-mini">Включен</span>
                                                @else
                                                    <span class="label label-warning label-mini">Выключен</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($module['status'] == 'off')
                                                    <a href="{{url('/modules/on/'.$module['title'])}}" class="btn btn-xs btn-primary">Включить</a>
                                                @else
                                                    <a href="{{url('/modules/off/'.$module['title'])}}" class="btn btn-xs btn-danger">Выключить</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

    <div class="modal fade " id="upload-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Default Modal Tittle</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('url'=>'module/upload','method'=>'POST', 'files'=>true)) !!}
                    <div class="control-group">
                        <div class="controls">
                            {!! Form::file('arch') !!}
                            <p class="errors">{!!$errors->first('image')!!}</p>
                            @if(Session::has('error'))
                                <p class="errors">{!! Session::get('error') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Отмена</button>
                    {!! Form::submit('Загрузить', array('class'=>'btn btn-success')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
