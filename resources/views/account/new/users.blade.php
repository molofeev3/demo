@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h1> Пользователи аккаунта</h1>
            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('user'))
            <div class="row">
                <div class="col-sm-5">
                    <section class="panel">
                        <header class="panel-heading">
                            Добавить нового
                        </header>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/users/new') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Имя</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail Aдресс</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role" class="col-md-4 control-label">Роль пользователя</label>
                                    <div class="col-md-6">
                                        <select name="role" id="role">
                                            <option value="3" selected>viewer</option>
                                            <option value="4">editor</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-user"></i> Зарегистрировать
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Пользователи
                        </header>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ФИО</th>
                                        <th>Email</th>
                                        <th>Дата создания</th>
                                        <th>Роль</th>
                                        @if(!\Illuminate\Support\Facades\Auth::user()->hasRole('viewer'))
                                            <th>Действия</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key=>$user)
                                        <tr>
                                            <td>{{$key}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->created_at}}</td>
                                            <td>{{$user->roles[0]['name']}}</td>
                                            <td>
                                                @if(!\Illuminate\Support\Facades\Auth::user()->hasRole('viewer'))
                                                    @if(!$user->hasRole('user'))
                                                        <form method="post" action="{{url('/user/'.$user->id)}}">
                                                            {{ csrf_field() }}
                                                            <button class="btn btn-danger btn-xs" type="submit" name="action" data-confirm="true" value="remove"><span class="fa fa-trash-o"></span></button>
                                                            <button class="btn btn-success btn-xs" type="submit" name="action" value="edit"><span class="fa fa-pencil"></span></button>
                                                        </form>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->
@endsection
