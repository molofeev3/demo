<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            @foreach(\App\Menu::all() as $item)
                @continue(isset($item->parent))
                @if(!empty(\App\Menu::where('parent', '=', $item->id)->get()->toArray()))
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa {{$item->ico}}"></i>
                            <span>{{$item->title}}</span>
                        </a>
                        <?php
                        $sub_items = \App\Menu::where('parent', '=', $item->id)->get()->toArray();
                        ?>
                        <ul class="sub">
                            @foreach($sub_items as $sub_item)
                                <li class="{{ Request::is($sub_item['url']) ? 'active' : '' }}" ><a  href="/{{$sub_item['url']}}">{{$sub_item['title']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a class="/{{ Request::is($item->url) ? 'active' : '' }}" href="{{url($item->url)}}">
                            <i class="fa {{$item->ico}}"></i>
                            <span>{{$item->title}}</span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->