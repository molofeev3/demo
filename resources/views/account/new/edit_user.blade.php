@extends('account.new.layout')

@section('content')
    @include('account.new.menu')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h1>Редактирование пользователя</h1>
                <div class="row">
                    <div class="col-sm-5">
                        <section class="panel">
                            <header class="panel-heading">
                                Данные пользователя
                            </header>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/'.$editing_user->id) }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Имя</label>
                                        <div class="col-md-6">
                                            <input id="name"  type="text" class="form-control" name="name" value="{{$editing_user->name}}">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail Aдресс</label>
                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{$editing_user->email}}">
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Пароль</label>
                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password">
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm" class="col-md-4 control-label">Подтверждение пароля</label>
                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="role" class="col-md-4 control-label">Роль пользователя</label>
                                        <div class="col-md-6">
                                            <select name="role" id="role">
                                                <option value="3" <?php echo ($editing_user->roles[0]['id'] == 3) ? 'selected' : ''; ?>>viewer</option>
                                                <option value="4" <?php echo ($editing_user->roles[0]['id'] == 4) ? 'selected' : ''; ?>>editor</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" name="action" value="save" data-confirm="true" class="btn btn-primary">
                                                <i class="fa fa-btn fa-user"></i> Edit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
        </section>
    </section>
    <!--main content end-->
@endsection
