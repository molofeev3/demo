<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <title>Ошибка</title>

    <!-- Bootstrap core CSS -->
    <link href="{{env('APP_URL')}}/new/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/new/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{env('APP_URL')}}/new/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{env('APP_URL')}}/new/css/style.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/new/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{env('APP_URL')}}/new/js/html5shiv.js"></script>
    <script src="j{{env('APP_URL')}}/new/s/respond.min.js"></script>
    <![endif]-->
</head>

<body class="body-500">

<div class="container">

    <section class="error-wrapper">
        <i class="icon-500"></i>
        <h1>Упс!</h1>
        <h2>Произошла ошибка</h2>
        <p class="page-500">{{$errormsg}} <a href="{{$link}}">Назад</a></p>
    </section>

</div>


</body>
</html>